<?php
header('Content-type: application/json');
$merchant_id = 38723;
$api_key = 'qIeA8e5iMIRioI6J';

$promo = isset($_GET['promo']) ? htmlspecialchars($_GET['promo']) : '';
$digitalContent = isset($_GET['digital_content']) ? htmlspecialchars($_GET['digital_content']) : '';
$project = isset($_GET['project']) ? (int)$_GET['project'] : '';

$requestArray = array (
    "settings" => array(
        "project_id" => $project
    ),
    "purchase" => array(
        "pin_codes" => array(
            "codes" => array(
                array(
                    "digital_content" => $digitalContent
                )
            )
        )
    ),
    "user" => array (
        "id" => array (
            "value" => microtime(false)
        ),
        "name" => array (
            "value" => "user"
        )
    ),
);
if($promo) {
    $requestArray['user']['attributes'] = array(
        "promo" => $promo
    );
}

$request = json_encode($requestArray);

$ch = curl_init('https://api.xsolla.com/merchant/merchants/'.$merchant_id.'/token');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Accept: application/json',
    'Authorization: Basic ' . base64_encode($merchant_id . ':' . $api_key)
));

$server_output = curl_exec($ch);
if (curl_errno($ch)) {
    print curl_error($ch);
} else {
    curl_close($ch);
}
echo $server_output;
