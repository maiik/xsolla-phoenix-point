var lif_static_data ={
  "groups":[
     {
        "id":6722,
        "name":"Boosters",
        "virtual_items":[
           {
              "id":151599,
              "sku":"10195",
              "name":"Combat skills raise booster (Power hour)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ca0e7504efd371d6207864b8997b078b.png",
              "description":"You'll be able to increase your combat and minor skill gains up to 2 times faster for 1 hour once every real 24 hours! Stacks with the food quality multiplier bonus.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":900,
              "vc_amount_without_discount":1000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":1,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151600,
              "sku":"10196",
              "name":"Crafting skills raise booster (Power hour)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f24f7de95dffcdb898a788e71ffa6779.png",
              "description":"You'll be able to increase your crafting and minor skill gains up to 2 times faster for 1 hour once every real 24 hours! Stacks with the food quality multiplier bonus.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":900,
              "vc_amount_without_discount":1000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":1,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6721,
        "name":"Consumables",
        "virtual_items":[
           {
              "id":151597,
              "sku":"20193",
              "name":"Gathering tools pack",
              "image_url":"//cdn.xsolla.net/img/misc/images/68ea959340255acde3d02fd829679836.png",
              "description":"Pack contains the following items (Quality level 50): Shovel, Hatchet, Pickaxe, Sickle and a Fishing Pole. These tools will not drop from your inventory upon death or being knocked out. Cannot be repaired and disappears once durability is lost.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":3600,
              "vc_amount_without_discount":4000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151598,
              "sku":"20194",
              "name":"Crafting tools pack",
              "image_url":"//cdn.xsolla.net/img/misc/images/9c60f1ebcd81ebd6b0c28e1a3f93cf69.png",
              "description":"Pack contains the following items (Quality level 50): Hammer, Saw, Knife, Crucible and Tongs. These tools will not drop from your inventory upon death or being knocked out. Cannot be repaired and disappears once durability is lost.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":3600,
              "vc_amount_without_discount":4000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6739,
        "name":"Services",
        "virtual_items":[
           {
              "id":151790,
              "sku":"50190",
              "name":"Ticket to Abella (additional)",
              "image_url":"//cdn.xsolla.net/img/misc/images/65a1905c0775ac456ee52cc0d076e13c.png",
              "description":"Give this to the Ferryman and he will transport you to Abella. A land of countless opportunities.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":9000,
              "vc_amount_without_discount":10000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":1,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6740,
        "name":"Insurances",
        "virtual_items":[
           {
              "id":151791,
              "sku":"30199",
              "name":"Death Insurance (5)",
              "image_url":"//cdn.xsolla.net/img/misc/images/57619b075f0ba0a3c44a5170419b0c5b.png",
              "description":"This insurance mitigates a part of your skills loss in an event of your death. Once activated, lasts for 24 real life hours. If skill loss upon your death is less than 5, then no skill loss will occur. If it is greater than 5, then it will occur, but will be lowered by 5. Cannot be used if the same or greater magnitude insurance is active.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":450,
              "vc_amount_without_discount":500,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151792,
              "sku":"30200",
              "name":"Death Insurance (10)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b15dda623768aa0ed1c2403eaec65fa0.png",
              "description":"This insurance mitigates a part of your skills loss in an event of your death. Once activated, lasts for 24 real life hours. If skill loss upon your death is less than 10, then no skill loss will occur. If it is greater than 10, then it will occur, but will be lowered by 10. Cannot be used if the same or greater magnitude insurance is active.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":900,
              "vc_amount_without_discount":1000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151793,
              "sku":"30201",
              "name":"Death Insurance (15)",
              "image_url":"//cdn.xsolla.net/img/misc/images/daab01d00972aa82077dd779023aad5d.png",
              "description":"This insurance mitigates a part of your skills loss in an event of your death. Once activated, lasts for 24 real life hours. If skill loss upon your death is less than 15, then no skill loss will occur. If it is greater than 15, then it will occur, but will be lowered by 15. Cannot be used if the same or greater magnitude insurance is active.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":1800,
              "vc_amount_without_discount":2000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151794,
              "sku":"30202",
              "name":"Death Insurance (20)",
              "image_url":"//cdn.xsolla.net/img/misc/images/9c805f1d08e28cb651906e1f6f574271.png",
              "description":"This insurance mitigates a part of your skill loss in the event of your death. Once activated, lasts for 24 real life hours. If skill loss upon your death is less than 20, then no skill loss will occur. If it is greater than 20, then it will occur, but will be lowered by 20. Cannot be used if the same or greater magnitude insurance is active.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":2700,
              "vc_amount_without_discount":3000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151795,
              "sku":"30203",
              "name":"Death Insurance (30)",
              "image_url":"//cdn.xsolla.net/img/misc/images/15107487edc12feb990c7174bf7a00f2.png",
              "description":"This insurance mitigates a part of your skills loss in an event of your death. Once activated, lasts for 24 real life hours. If skill loss upon your death is less than 30, then no skill loss will occur. If it is greater than 30, then it will occur, but will be lowered by 30. Cannot be used if the same or greater magnitude insurance is active.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":4500,
              "vc_amount_without_discount":5000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6724,
        "name":"Hats",
        "virtual_items":[
           {
              "id":151601,
              "sku":"60153",
              "name":"Herald Hat (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1bbd26d89318f1aafe3307d3e4c0b92b.png",
              "description":"A rather fanciful hat made of blue silk with a rough canvas band fitted tightly around the head. Hats like these are typically worn by small-time merchants, town criers, and scribes.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":1,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151609,
              "sku":"60162",
              "name":"Eaglehunter's Hat (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d3fd10f9b51d2439392ed9e12d7cf750.png",
              "description":"A hat made of crimson goat leather with some kind of dark fur with red spots – probably dog – sewn around it.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151608,
              "sku":"60161",
              "name":"Khoors Hat (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6c2c518e7a09db42f52839110f1aa4e9.png",
              "description":"A skullcap made of horse hide stitched roughly and irregularly. Its warm, sturdy felt lining is turned slightly inside-out to resemble a fur border.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151607,
              "sku":"60160",
              "name":"Wool Hat (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/09a5e0a238190a8fe6593a831e964565.png",
              "description":"A tall fur-lined hat with a leather hood rakishly cocked to one side. ",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":1,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151606,
              "sku":"60159",
              "name":"Duke's Hat (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5557a5cfb5bb659b7163c421c861bf40.png",
              "description":"A skullcap with fur on the inside. A short tail hangs down from the back to carefully cover the nape of the wearer's neck.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":22500,
              "vc_amount_without_discount":25000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151605,
              "sku":"60158",
              "name":"Peasant's Hat (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ff240576f58e71ce55fda086cce39c48.png",
              "description":"A hat with ear flaps made of coarse linen and fringed with wool. It's not much to look at, but it's warm.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":7200,
              "vc_amount_without_discount":8000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151604,
              "sku":"60156",
              "name":"Farmer's Hat (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d87b7eeeaa1c611729c0b6273303d7e0.png",
              "description":"Simple, rough headwear – a white canvas hood sewn onto a wide-brimmed straw hat as a kind of lining.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151603,
              "sku":"60155",
              "name":"Raider's Hat (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1d51e3c2487b36061cffdace0759eeed.png",
              "description":"Hats with bent brims like this one are frequently worn by traveling merchants. Its soft leather lining is solid enough to protect against wind and rain.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151602,
              "sku":"60154",
              "name":"Dandy Hat (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/afd4c84bcd641c29bfaf7b3bb88f73fa.png",
              "description":"A jaunty hat with a bent brim made of thick, aged leather.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":9000,
              "vc_amount_without_discount":10000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151610,
              "sku":"60163",
              "name":"Cap (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d537fd72a488ff474c0705c251c67a6c.png",
              "description":"A tall hat made of ox hide. It has been poorly dyed with brown and red stripes.",
              "long_description":"Skin can be applied on: Fur Hat\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":7200,
              "vc_amount_without_discount":8000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6725,
        "name":"Boots",
        "virtual_items":[
           {
              "id":151611,
              "sku":"60164",
              "name":"Raider's Boots (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/2e0fd43462fe53c5b9e63aa863ac7fbe.png",
              "description":"Unimpressive-looking pointed shoes with boiled leather soles. The thin rope laces literally fall apart in your hands, leaving red fibers on your fingers.",
              "long_description":"Skin can be applied on: Primitive Boots\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151612,
              "sku":"60165",
              "name":"High Boots",
              "image_url":"//cdn.xsolla.net/img/misc/images/997f6762224dde2dc31e68778e479132.png",
              "description":"These boots are made of cow leather. They are sturdy, shapeless, and have enormous bootlegs that slap against the calves with each step.",
              "long_description":"Skin can be applied on: Primitive Boots\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":9000,
              "vc_amount_without_discount":10000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151613,
              "sku":"60166",
              "name":"Riding Boots (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6555b6c3ffbe67a5eccefaa1a7a975da.png",
              "description":"Boots with bent tips that fit the foot snugly. Indispensable footwear among Khoors horsemen.",
              "long_description":"Skin can be applied on: Primitive Boots\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151614,
              "sku":"60167",
              "name":"Felt Boots (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f05978d28073c2d7dd11a12e5f9542e5.png",
              "description":"Boots like these are made of pieces of thick felt. They are surprisingly warm and almost waterproof and can last for years.",
              "long_description":"Skin can be applied on: Primitive Boots\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":9000,
              "vc_amount_without_discount":10000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151615,
              "sku":"60168",
              "name":"Rag Boots",
              "image_url":"//cdn.xsolla.net/img/misc/images/10a798876ff7ef239a62a0d2ab13316e.png",
              "description":"Shoes with no tips made of coarse linen and leather scraps.",
              "long_description":"Skin can be applied on: Primitive Boots\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":9000,
              "vc_amount_without_discount":10000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151616,
              "sku":"60169",
              "name":"Noble Boots (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/96b729341f8c4c595a2953490d835603.png",
              "description":"Short shoes with thin straps. Simple, but with a pretense to elegance.",
              "long_description":"Skin can be applied on: Primitive Boots\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":7200,
              "vc_amount_without_discount":8000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151617,
              "sku":"60170",
              "name":"Peasant's Boots (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/39e802d4e5a055890b58c08a798349ac.png",
              "description":"Rough peasant shoes with wooden heels that make a loud clacking sound when you walk.",
              "long_description":"Skin can be applied on: Primitive Boots\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":7200,
              "vc_amount_without_discount":8000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6737,
        "name":"Weapon Skins",
        "virtual_items":[
           {
              "id":151661,
              "sku":"60143",
              "name":"Two-Handed Northern Sword",
              "image_url":"//cdn.xsolla.net/img/misc/images/c46c5608e353227f9a08554a7090208f.png",
              "description":"A two-handed sword with a long hilt wrapped in tawed straps. It has a short ricasso and a sharp, heavy blade with deeply engraved runes.",
              "long_description":"Skin can be applied on: Zweihaender\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151662,
              "sku":"60144",
              "name":"Landsknecht's Sword",
              "image_url":"//cdn.xsolla.net/img/misc/images/f45efccb14ed5e879dacf705c1d7352d.png",
              "description":"Landsknecht's Sword A simple zweihander with short, three-edged fangs. It is surprisingly evenly sharpened. It is simply constructed, but made with noticeable skill and has a certain ineffable elegance to its appearance. The base of the blade has the words 'ad patres' carved into it — a curt message to anyone who might stand in the way of this weapon's owner.",
              "long_description":"Skin can be applied on: Zweihaender\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151663,
              "sku":"60145",
              "name":"Mercenary's Two-Handed Sword",
              "image_url":"//cdn.xsolla.net/img/misc/images/9ba30d304d67a3b60dce63d6d4a92e88.png",
              "description":"Mercenaries like these swords for their simple shape and perfect balance — the lead-filled hilt excellently balances the heavy blade with its long ricasso. 'Ad hominem,' intone the slender letters carved into the sword — an irony that perhaps only the sword's owner can appreciate.",
              "long_description":"Skin can be applied on: Zweihaender\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":18000,
              "vc_amount_without_discount":20000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151664,
              "sku":"60146",
              "name":"Slumbering Sword",
              "image_url":"//cdn.xsolla.net/img/misc/images/7cae8a0a7a54310789071f5d7e75efed.png",
              "description":"The Eye of the Sleeper can be glimpsed in the gilded guard of this two-handed sword. The phrase 'ultima ratio' is carved into the blade in ornamental script that is smooth and soft-edged, as though pressed into the surface of the sword.",
              "long_description":"Skin can be applied on: Zweihaender\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":40500,
              "vc_amount_without_discount":45000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151665,
              "sku":"60147",
              "name":"Adorned Two-Handed Sword",
              "image_url":"//cdn.xsolla.net/img/misc/images/9c62887f97a27d9da82f6252f1fdc254.png",
              "description":"This sword is a copy of the legendary lost sword known as 'Terskel.' Strips of studded leather form an intricate pattern on the hilt, and a gilded hand gripping the horn Rivelim form the fangs of this sword. The ornate guard is covered in a thin layer of gilt.",
              "long_description":"Skin can be applied on: Zweihaender\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":22500,
              "vc_amount_without_discount":25000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151666,
              "sku":"60148",
              "name":"Wildman's Axe",
              "image_url":"//cdn.xsolla.net/img/misc/images/ea777e3970e32c8b77601a7ebef3a74b.png",
              "description":"A massive, roughly forged axe on a long, light pole. It has a small butt and a curved blade with northern runes writ upon it in the shape of a crescent moon. Its rustic appearance is deceptive — this is a reliable weapon that is easy to wield and quite powerful for its weight.",
              "long_description":"Skin can be applied on: Broad Axe\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":22500,
              "vc_amount_without_discount":25000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151667,
              "sku":"60149",
              "name":"Small Poleaxe",
              "image_url":"//cdn.xsolla.net/img/misc/images/6b3f3ffa9be4b4bd04b5c9183d427e56.png",
              "description":"This axe with a handle wrapped in strips of leather and a curved blade is not without a certain coarse, idiosyncratic beauty. Its charm may lie in the simple angular pattern carved inside of the axe — a hallmark of the northern blacksmiths.",
              "long_description":"Skin can be applied on: Broad Axe\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151668,
              "sku":"60150",
              "name":"Bruiser's Axe",
              "image_url":"//cdn.xsolla.net/img/misc/images/cbbc7d305af8d17d2b6c622d5e47dc8b.png",
              "description":"A Slavard axe with a beveled butt and a long, straight, thick blade. In the northern lands this axe is known as a 'bruiser's axe,' as though calling attention to the weapon's personality and status.",
              "long_description":"Skin can be applied on: Broad Axe\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":18000,
              "vc_amount_without_discount":20000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151669,
              "sku":"60151",
              "name":"Enchanted Axe",
              "image_url":"//cdn.xsolla.net/img/misc/images/85596dbe070913318899943256a0fe2c.png",
              "description":"An unusually thin axe handle on a straight pole wrapped in iron and leather strips. The blade is bronze-plated and covered in shallow, yet clearly visible runes. Like a spell whispered over the weapon by an enchanter, they protect the warrior from doom and misfortune.",
              "long_description":"Skin can be applied on: Broad Axe\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":22500,
              "vc_amount_without_discount":25000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151670,
              "sku":"60152",
              "name":"Talismanic Axe",
              "image_url":"//cdn.xsolla.net/img/misc/images/9415dec77c6cf94ab4d849425cf29dfa.png",
              "description":"Compared to the Slavards' artlessly simple weapons this axe is especially good. The dragon head on the butt with its whimsically winding gilded pattern is not just a frame for the sharp blade made from tempered steel – it is an ancient warrior's talisman, a special, almost palpable force that protects the axe's owner.",
              "long_description":"Skin can be applied on: Broad Axe\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151836,
              "sku":"61158",
              "name":"Conquerors' Glaive",
              "image_url":"//cdn.xsolla.net/img/misc/images/f773781ffead4fc5b478b3deb14e5c79.png",
              "description":"Whether re-forged from a sword or an axe, this glaive has an odd shape and is adorned in the northern tradition. Its long, contracted blade is tipped with the image of a god – apparently the trickster Gloom, beloved by raiders. The hilt has been fortified with a metal sleeve and strips of coarse leather.",
              "long_description":"Skin can be applied on: Glaive\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151837,
              "sku":"61159",
              "name":"Banensmerte glaive",
              "image_url":"//cdn.xsolla.net/img/misc/images/c268359d07385c0d401100219d2ddcfd.png",
              "description":"The name of this glaive means 'The Way of Pain' in the Slavard tongue. The blade's unusual shape has been augmented with dragon's teeth and a long, sharpened hook that is equally good for parrying enemy blows and disemboweling foes.",
              "long_description":"Skin can be applied on: Glaive\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":18000,
              "vc_amount_without_discount":20000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151838,
              "sku":"61161",
              "name":"Amathe's generocity glaive",
              "image_url":"//cdn.xsolla.net/img/misc/images/b6ccf9f89121a7be9be897641f031a58.png",
              "description":"The blade is adorned with an image of the sun-faced goddess holding a sacrificial sickle of the kind best used to pull horsemen from their steeds or sever tendons. The pole is wrapped in tooled leather.",
              "long_description":"Skin can be applied on: Glaive\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":22500,
              "vc_amount_without_discount":25000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151839,
              "sku":"61162",
              "name":"Ambassador's glaive",
              "image_url":"//cdn.xsolla.net/img/misc/images/4c2ef7a5faac4bee76187f57d271a8e8.png",
              "description":"This weapon resembles a tongue of flame frozen into a hunk of tempered steel. Bronze and gilt play upon the wide, sharp blade on a pole wrapped in morocco straps.",
              "long_description":"Skin can be applied on: Glaive\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":40500,
              "vc_amount_without_discount":45000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151840,
              "sku":"61163",
              "name":"Sleeper's Glaive",
              "image_url":"//cdn.xsolla.net/img/misc/images/9d6fa34be942fcfbff5ce9d51649ac16.png",
              "description":"A long, triangular blade with a pristinely smooth, razor-sharp edge. Glaive is adorned with the Eye, crafted out of slivers of gilt steel. Yew wood and thin strips of leather lead to an austere yet elegant pommel finish",
              "long_description":"Skin can be applied on: Glaive\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":40500,
              "vc_amount_without_discount":45000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6734,
        "name":"Leather Skins",
        "virtual_items":[
           {
              "id":151639,
              "sku":"60087",
              "name":"Novice Leather Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/2bc2e75124c01017cdf41a3273e92233.png",
              "description":"A sewn cap. It is soft and stuffed with linen rags.",
              "long_description":"Skin can be applied on: Novice Leather Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151756,
              "sku":"60102",
              "name":"Heavy Leather Breastplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e2027ccf40c761b72aacf6168789834b.png",
              "description":"This leather armor is somewhat reminiscent of plate mail – it has plates covering the chest and shoulders and a gorget to protect the neck, and it weighs a little less than plate mail.",
              "long_description":"Skin can be applied on: Heavy Leather Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151757,
              "sku":"60103",
              "name":"Heavy Leather Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/30b862795d2909c89e2bf594598dacb4.png",
              "description":"Each of these bracers are assembled from a dozen steel plates sewn into leather sleeves covering the forearms. They are tightened using laces.",
              "long_description":"Skin can be applied on: Heavy Leather Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151758,
              "sku":"60104",
              "name":"Heavy Leather Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6f636028019109da38fcb86152a06705.png",
              "description":"Heavy, well-made gauntlets made of boiled leather. They are reinforced with steel scales sewn into the top. The fingers are made of thin, light leather.",
              "long_description":"Skin can be applied on: Heavy Leather Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151759,
              "sku":"60105",
              "name":"Heavy Leather Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c46f883a1a7038242c5f70bf47737949.png",
              "description":"Broad leggings with barely noticeable strips of riveted leather. They are well made and soft, yet durable.",
              "long_description":"Skin can be applied on: Heavy Leather Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151760,
              "sku":"60106",
              "name":"Heavy Leather Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ae54731040be00b369003f1f0bb87381.png",
              "description":"Heavy boots made of matte leather. They are double-stitched and painstakingly made. Both the boots themselves and the straps on them are adorned with buckles and rivets in various shapes.",
              "long_description":"Skin can be applied on: Heavy Leather Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151761,
              "sku":"60109",
              "name":"Royal Leather Brestplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/96fe3cf91e79f3f4f21bff53106531ba.png",
              "description":"A leather cuirass with steel plates sewn into it and a blood-red collar made of thin leather. Buckles and tiny fasteners, dozens of rivets – it's all gilded and shines with a noble luster.",
              "long_description":"Skin can be applied on: Royal Leather Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151762,
              "sku":"60110",
              "name":"Royal Leather Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/be64fb0d33b87bcd29b7943543e4d22a.png",
              "description":"Thick plates of tempered steel are sewn beneath dyed red leather covered with tooled patterns.",
              "long_description":"Skin can be applied on: Royal Leather Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151763,
              "sku":"60111",
              "name":"Royal Leather Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4d8342a074500a22e5fa0760441c9520.png",
              "description":"These solid, expensive-looking gauntlets are covered with thin plates of gilded steel. The fingers are made of strips of black boiled leather, and the insides of the gauntlets are made of soft, red satin.",
              "long_description":"Skin can be applied on: Royal Leather Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151764,
              "sku":"60113",
              "name":"Royal Leather Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ec62233d86922cdc9da96cf58a56bd3f.png",
              "description":"Boots made of expensive, soft leather. They are decorated with gilded iron buckles and dark red leather straps.",
              "long_description":"Skin can be applied on: Royal Leather Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151796,
              "sku":"60112",
              "name":"Royal Leather Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/cf0aa12107ba86e539f07f8d215a2ab4.png",
              "description":"Thick laced leggings made of specially treated leather. They are covered with riveted leather patches to protect the knees and calves.",
              "long_description":"Skin can be applied on: Royal Leather Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151816,
              "sku":"60246",
              "name":"Royal Leather Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/91e4ca3bba181916073f157dff2a6ffd.png",
              "description":"A pointed helmet reminiscent of a Khoors tent. The top part is forged, and the helmet itself is made of several layers of thick studded leather and outfitted with a steel nose-guard.",
              "long_description":"Skin can be applied on: Heavy Leather Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151817,
              "sku":"60247",
              "name":"Royal Leather Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ccace5b930ecaecf376477169db6d8b1.png",
              "description":"A tastefully made and decorated calfskin overcoat with a broad crimson belt and a felt doublet that can absorb hacking blows. It is reinforced with studded-leather and steel plates.",
              "long_description":"Skin can be applied on: Heavy Leather Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151818,
              "sku":"60248",
              "name":"Royal Leather Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/77ab75bffcf5fd1cde6c5ece2a758909.png",
              "description":"A pair of leather bracers made of pieces of boiled leather riveted together. They are stitched for greater sturdiness. The elbows are protected with steel plates.",
              "long_description":"Skin can be applied on: Heavy Leather Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151819,
              "sku":"60249",
              "name":"Royal Leather Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4cff065c9abc74b870478b2624f145cb.png",
              "description":"Leather gauntlets with soft cuffs. They are lined with black silk sewn with silvery thread. They fit the hand... well, like a glove.",
              "long_description":"Skin can be applied on: Heavy Leather Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151820,
              "sku":"60250",
              "name":"Royal Leather Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/fc58c642919a60d980dfaee6b7ec8384.png",
              "description":"These leather greaves are made of layers of lacquered leather riveted together in an overlapping pattern. The abundance of rivets, tooled decorations, and patterned trim make them uniquely elegant and beautiful items.",
              "long_description":"Skin can be applied on: Heavy Leather Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151755,
              "sku":"60099",
              "name":"Regular Leather Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b0993e263b17a591534118b031693b71.png",
              "description":"Leather boots with thick, spongy soles. Instead of laces, the calves are wrapped in several soft straps.",
              "long_description":"Skin can be applied on: Regular Leather Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151754,
              "sku":"60098",
              "name":"Regular Leather Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/7eede7963b03c6ea91b5f0b04281222d.png",
              "description":"These leggings are made of coarse leather and reinforced with riveted patches in the most vulnerable places.",
              "long_description":"Skin can be applied on: Regular Leather Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151640,
              "sku":"60094",
              "name":"Regular Leather Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6753f1dd24c92fbc28b308cf9a738387.png",
              "description":"A leather helm with a band on the inside. Iron scale is sewn inside the nasel.",
              "long_description":"Skin can be applied on: Regular Leather Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151641,
              "sku":"60101",
              "name":"Heavy Leather Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e35b5cfb53d534a58bafe7eab2a91667.png",
              "description":"This heavy helm only looks like pure leather – both the helm and its flaps are actually made of steel plates riveted together and covered with leather and tooled designs.",
              "long_description":"Skin can be applied on: Heavy Leather Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151642,
              "sku":"60108",
              "name":"Royal Leather Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a9a166ee54f5f40778542686435a2d15.png",
              "description":"A heavy helm made of steel with richly decorated, tooled leather sewn around it. The edging and patterns are gilded.",
              "long_description":"Skin can be applied on: Royal Leather Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":44100,
              "vc_amount_without_discount":49000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151675,
              "sku":"70114",
              "name":"Royal Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d7506e338a0911bd3357bb0d7863a71e.png",
              "description":"Armor of this quality is rare indeed. Behind every suit of armor such as this is the story of a strong, distinguished warrior. Few are worthy to wear it.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151690,
              "sku":"70093",
              "name":"Novice Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b1fb5f50d1b964cfff0b607d958205de.png",
              "description":"Unpretentious armor for spearmen and bowmen. It provides little defense, but does not restrict the wearer's movements.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151691,
              "sku":"70100",
              "name":"Regular Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1457cbd2fc01ff3f41630a80ab02f9bd.png",
              "description":"Ordinary armor of the type worn by poor mercenaries, armed travelers, and highwaymen. Easy to fix and reasonably priced.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151692,
              "sku":"70107",
              "name":"Heavy Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/fc5aa5a6206a843d9d0f63b4120b3709.png",
              "description":"Leather armor like this was encountered quite rarely in the good old days. It was typically worn by petty noblemen and mercenaries with connections. Nowadays anyone with enough gold can get their hands on a similar suit.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151746,
              "sku":"60088",
              "name":"Novice Leather Breastplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/dae83841a245ac74a2353f30a7812201.png",
              "description":"Simple, light leather armor with canvas sleeves and a woolen hood in case of rain or snow.",
              "long_description":"Skin can be applied on: Novice Leather Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151747,
              "sku":"60089",
              "name":"Novice Leather Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ab5766aa2e5c3048be4ea6184fe46d21.png",
              "description":"Shapeless bracers sewn together from scraps of old leather. They are lined with small pieces of cloth scraps.",
              "long_description":"Skin can be applied on: Novice Leather Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151748,
              "sku":"60090",
              "name":"Novice Leather Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/15363c6d89346a569d9e94aa560e5980.png",
              "description":"These shapeless gauntlets are made of pigskin. They might not look like much, but they do protect the arms.",
              "long_description":"Skin can be applied on: Novice Leather Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151749,
              "sku":"60091",
              "name":"Novice Leather Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/00d226aa09052bd25f2b51feb3ee81a2.png",
              "description":"Simple canvas pants reinforced with strips of leather. They are light and durable to a certain point.",
              "long_description":"Skin can be applied on: Novice Leather Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151750,
              "sku":"60092",
              "name":"Novice Leather Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f23401253b17a6021642d7c2b2217296.png",
              "description":"Shoes with wooden soles held on with fish glue. They are made of pigskin coated with lard.",
              "long_description":"Skin can be applied on: Novice Leather Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151751,
              "sku":"60095",
              "name":"Regular Leather Breastplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/347739b02e7b9f03cbf9ec8c44c2488a.png",
              "description":"A cuirass made of boiled leather with riveted, lined steel plates.",
              "long_description":"Skin can be applied on: Regular Leather Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151752,
              "sku":"60096",
              "name":"Regular Leather Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f8e4e8ab6da1fccf08f9f310a8ede83a.png",
              "description":"Thick bracers made of three layers of boiled leather riveted along the edges. They are fastened to the arm with two broad straps.",
              "long_description":"Skin can be applied on: Regular Leather Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151753,
              "sku":"60097",
              "name":"Regular Leather Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/de54f8e4caaa0422d7fb8c842711c72b.png",
              "description":"Gauntlets made of solid leather with long, broad cuffs designed to resemble Gottlung plate gauntlets. The fingers are made of dark leather and are more finely crafted.",
              "long_description":"Skin can be applied on: Regular Leather Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151821,
              "sku":"60251",
              "name":"Royal Leather Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ad69b05eb3f62dc00cdd70319698af9e.png",
              "description":"Short dark-red morocco boots. They appear deceptively light, but penetrating them isn't easy – their tops are reinforced with two layers of studded leather that reliably protect the warrior's legs.",
              "long_description":"Skin can be applied on: Heavy Leather Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6735,
        "name":"Padded Skins",
        "virtual_items":[
           {
              "id":151643,
              "sku":"60115",
              "name":"Novice Padded Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/60f7b5a89341402b3a04d35ef4ad4d2e.png",
              "description":"A coarse, quilted, laced helm. It is stuffed with something that's not quite oakum and not quite fur and is permeated with salt to the point of crackling.",
              "long_description":"Skin can be applied on: Novice Padded Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151776,
              "sku":"601291",
              "name":"Heavy Padded Helm - Boar Trophy (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/0d5dd839894235f543e5108fb99e5e45.png",
              "description":"The head of this old, battle-scarred boar makes an excellent helm – it has solid lining with strips of leather sewn into it and a steel skull cap with a half-mask that covers the warrior's eyes and nose.",
              "long_description":"Skin can be applied on: Heavy Padded Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":54000,
              "vc_amount_without_discount":60000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151777,
              "sku":"60130",
              "name":"Heavy Padded Breastplate (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/50080dfa087f5569e66a85ddc642967a.png",
              "description":"A quilted tunic. It is sturdy and has a fur lining soaked in salt casks. It is covered with ribbons of thick, hard leather in a criss-cross pattern.",
              "long_description":"Skin can be applied on: Heavy Padded Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151778,
              "sku":"60131",
              "name":"Heavy Padded Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/734aa90e93c37b2439ab770719f978f4.png",
              "description":"Broad bracers made of tanned leather with a cloth lining.",
              "long_description":"Skin can be applied on: Heavy Padded Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151780,
              "sku":"60133",
              "name":"Heavy Padded Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d0c08cd872b5b0e487329b158a34cd29.png",
              "description":"Puffy cloth pants sewn from three layers of fabric with oakum and straw stuffed between them. They're warm, and they can take a hit well.",
              "long_description":"Skin can be applied on: Heavy Padded Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151781,
              "sku":"60134",
              "name":"Heavy Padded Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/041d80b808aa285705a5fab9686312ed.png",
              "description":"These boots look shapeless, but on the inside they have high wooden soles, hard leather legs that cling to the calves, and fur lining.",
              "long_description":"Skin can be applied on: Heavy Padded Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151782,
              "sku":"60137",
              "name":"Royal Padded Brestplate (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/9c9f331d2db54d10a2c56b55dfd3ce05.png",
              "description":"Heavy padded armor made of three layers of dyed linen. Under a lining of black boar fur hides coarse, solid fabric with strips of leather sewn into it.",
              "long_description":"Skin can be applied on: Royal Padded Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151783,
              "sku":"60138",
              "name":"Royal Padded Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5020494001156c7739e4a5b43913a9c6.png",
              "description":"Bracers the color of frozen blood. They are made of leather, stuffed with matted wool, and carefully riveted in a circle.",
              "long_description":"Skin can be applied on: Royal Padded Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151784,
              "sku":"60139",
              "name":"Royal Padded Gauntlets (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d560ea21c89cc9520b899759e8bf1a31.png",
              "description":"Judging from their softness and flexibility, these gauntlets are made of calfskin that has been treated and prepared in a special way. On the inside they are sewn like padded armor and lined with numerous rivets that are hard to spot in the thick fur lining.",
              "long_description":"Skin can be applied on: Royal Padded Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151785,
              "sku":"60140",
              "name":"Royal Padded Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ee87596e9b79dd5b9d8b11f90563963d.png",
              "description":"Thick linen leggings made from four layers of crimson fabric with layers of horsehair and matted wool in-between.",
              "long_description":"Skin can be applied on: Royal Padded Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151786,
              "sku":"60141",
              "name":"Royal Padded Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1aaf3444a95701b3b39dff3987d494c9.png",
              "description":"These boots are made of solid, rough leather with red fabric sewn around them. They are insulated with thick fur.",
              "long_description":"Skin can be applied on: Royal Padded Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151822,
              "sku":"60253",
              "name":"Royal Padded Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6ebd2f1eefc78429b44a0894f9e96291.png",
              "description":"A skullcap made of tempered steel with a quilted aventail. Double-stitched sturdy silk thread combines two layers of soft leather lined with horsehair and a fine chainmail netting that cannot be seen from the side.",
              "long_description":"Skin can be applied on: Heavy Padded Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151823,
              "sku":"60254",
              "name":"Royal Padded Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/610c223d0cd2d14edae4e881d9a8e20f.png",
              "description":"A light quilted jacket that fits the torso perfectly and is carefully conceived – behind the rabbit-fur lining is a layer of sticky boiled leather, and the most vulnerable areas are protected by thin steel plates.",
              "long_description":"Skin can be applied on: Heavy Padded Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151824,
              "sku":"60255",
              "name":"Royal Padded Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/01dd4ab7feeb58da07214791b8bd7671.png",
              "description":"Magnificently crafted quilted bracers adorned with stripes of blue-black leather.",
              "long_description":"Skin can be applied on: Heavy Padded Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151825,
              "sku":"60256",
              "name":"Royal Padded Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/730146995ca5bb0cdbe3ca8f291df769.png",
              "description":"Gauntlets made from several layers of coarse linen lined with pieces of tanned leather. Sturdy enough to stop a spear or soften a blow from a club.",
              "long_description":"Skin can be applied on: Heavy Padded Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151826,
              "sku":"60257",
              "name":"Royal Padded Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5339086abdaca4bdae06e25a055b1322.png",
              "description":"Quilted greaves. Strips of yarn, horsehair, and some chainmail netting can be felt through the dark-blue fabric. The greaves nevertheless provide excellent protection.",
              "long_description":"Skin can be applied on: Heavy Padded Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151775,
              "sku":"60127",
              "name":"Regular Padded Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a90513aac4c1c387730b73a2b6d84775.png",
              "description":"Tall boots. They're warm, but hastily made. It's almost as if they're inside out, with seams on the outside and fur on the inside.",
              "long_description":"Skin can be applied on: Regular Padded Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151774,
              "sku":"60126",
              "name":"Regular Padded Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ad8b772350407a14a89c94f4fcb4e7cc.png",
              "description":"Insulated cloth pants with rhombuses sewn onto the knees and calves.",
              "long_description":"Skin can be applied on: Regular Padded Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151644,
              "sku":"60122",
              "name":"Regular Padded Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/2e0f3eaa5fd7026e657e4688b33e8a1a.png",
              "description":"A laced quilted helm with ear flaps. It is made thicker with waste cloth around the top of the head, nape of the neck, and temples and stuffed with matted wool.",
              "long_description":"Skin can be applied on: Regular Padded Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151645,
              "sku":"60129",
              "name":"Heavy Padded Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a14af645114ca5b066482371d454f95e.png",
              "description":"A helm made of strips of coarse, uneven iron riveted onto a thick quilted lining stuffed with oakum and fur. The warrior's face is protected by an iron half-mask.",
              "long_description":"Skin can be applied on: Heavy Padded Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151646,
              "sku":"60136",
              "name":"Royal Padded Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e8421d4e5cd7edcd8cfd397771a85173.png",
              "description":"This helm's steel ribs are hidden under a boar's head stuffed full of scraps of cloth and leather.",
              "long_description":"Skin can be applied on: Royal Padded Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":44100,
              "vc_amount_without_discount":49000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151676,
              "sku":"70142",
              "name":"Royal Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/497d64b0eb530cafcee3e912b665d552.png",
              "description":"A popular kind of armor among lightly armed northern warriors. The boar's head is a symbol of rage, and the crimson color is also a symbol, although everyone understands it in their way.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151693,
              "sku":"70121",
              "name":"Novice Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ac03a2c80d59b43687c4a81eb5d09a1b.png",
              "description":"Rough, dirty rags that might protect against arrows or maybe clubs... if that.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151694,
              "sku":"70128",
              "name":"Regular Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b2bfa5c628e3b489c0041c77e0b99f7c.png",
              "description":"Quilted armor insulated with fur and reinforced here and there with strips and pieces of leather. It isn't much to look at, but it's good at stopping arrows and absorbing club blows. It is solidly made from coarse fabric – not every sword can cut through quilted armor like this in a single blow.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151695,
              "sku":"70135",
              "name":"Heavy Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c5d8f79212a14548f82d76091a245003.png",
              "description":"This suit of armor is light and preserves warmth for a long time. It is also soft and sturdy. Swords and axes basically get stuck in it with each blow.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151765,
              "sku":"60116",
              "name":"Novice Padded Breastplate (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ccb7405cb937eff3d921459ea88dfdb1.png",
              "description":"A puffy quilted jacket stitched with horsehair. It is stuffed with scrap fabric and oakum and gives off a light, musty aroma.",
              "long_description":"Skin can be applied on: Novice Padded Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151766,
              "sku":"60117",
              "name":"Novice Padded Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ab2de0cd352f341b693ae175c44044b2.png",
              "description":"These aren't bracers at all – they're linen rags fastened on with thin straps with no buckles.",
              "long_description":"Skin can be applied on: Novice Padded Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151767,
              "sku":"60118",
              "name":"Novice Padded Gauntlets (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/cf54167d28f9f0161a957e42fba9abff.png",
              "description":"Quilted fingerless gloves. They make a crunching sound when you make a fist in them – there must be straw inside. They might look rustic, but they provide some protection against arrows.",
              "long_description":"Skin can be applied on: Novice Padded Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151768,
              "sku":"60119",
              "name":"Novice Padded Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8518b78228236e195e6fa6511ce62db6.png",
              "description":"Linen pants with bindings sewn along rough, wobbly seams in a criss-cross pattern. There are patches made of folded-over pieces of sackcloth on the knees.",
              "long_description":"Skin can be applied on: Novice Padded Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151769,
              "sku":"60120",
              "name":"Novice Padded Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/0d15ed9fa36df0cb20f3ec9194ff0bbf.png",
              "description":"Short boots made of solid fabric soaked in lard.",
              "long_description":"Skin can be applied on: Novice Padded Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151770,
              "sku":"601221",
              "name":"Regular Padded Helm - Boar Trophy (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5512ba13d544919bd398d06aa6a2b653.png",
              "description":"A puffy quilted helm stuffed with cloth scraps and matted wool hidden under a raggedy boar's head with it's eyes sewn shut.",
              "long_description":"Skin can be applied on: Heavy Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":27000,
              "vc_amount_without_discount":30000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151772,
              "sku":"60124",
              "name":"Regular Padded Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d0b421ba9ee7322f15194be012b4b7a5.png",
              "description":"Pieces of white linen cloth folded into several layers and carefully sewn into rather sturdy bracers.",
              "long_description":"Skin can be applied on: Regular Padded Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151773,
              "sku":"60125",
              "name":"Regular Padded Gauntlets (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5bc42a18a1cb917e0e68bd250656dd40.png",
              "description":"Quilted gauntlets insulated with boar fur. Their solid leather lining provides good protection for the hands and preserves warmth.",
              "long_description":"Skin can be applied on: Regular Padded Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151827,
              "sku":"60258",
              "name":"Royal Padded Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1a0851c93ca5293ac2eab0a3c500c7ad.png",
              "description":"Boots made from thin leather covered with rhombuses of quilted linen. They would be weightless if not for the steel plates sewn inside of them to protect the calf and instep.",
              "long_description":"Skin can be applied on: Heavy Padded Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6733,
        "name":"Chainmail Skins",
        "virtual_items":[
           {
              "id":151635,
              "sku":"60059",
              "name":"Light Chainmail Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/20eaa0aa575f8fe964497aae21a13f08.png",
              "description":"A helm made of a thin chain netting. Something like sackcloth has been sewn onto it.",
              "long_description":"Skin can be applied on: Light Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151736,
              "sku":"60074",
              "name":"Heavy Chainmail Breastplate (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4a8ed0d44e8bfed36004dca53b273b20.png",
              "description":"This chain mail is made from tiny steel rings. The rings are woven together so thickly that they resemble fish scales. The long skirts and sleeves are adorned with runes and carved bone talismans.",
              "long_description":"Skin can be applied on: Heavy Chainmail Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151737,
              "sku":"60075",
              "name":"Heavy Chainmail Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/28854810ee44435d0e7e163422cde4f8.png",
              "description":"Bracers made of cow leather with a solid netting of forged steel rings.",
              "long_description":"Skin can be applied on: Heavy Chainmail Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151738,
              "sku":"60076",
              "name":"Heavy Chainmail Gauntlets (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ea842836058730e4b25fea504ee51168.png",
              "description":"Woven chain gauntlets with a soft wolf-fur lining.",
              "long_description":"Skin can be applied on: Heavy Chainmail Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151739,
              "sku":"60077",
              "name":"Heavy Chainmail Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d5e5169f1dbae04276b320896fe71c03.png",
              "description":"Pants made of dyed canvas. They look deceptively light, but on the inside they are lined with wolf fur and thin strips of chain mail to protect the calves and thighs against blows.",
              "long_description":"Skin can be applied on: Heavy Chainmail Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151740,
              "sku":"60078",
              "name":"Heavy Chainmail Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/0a6c11ed302191ea0b5f094aaf8e178c.png",
              "description":"Chain mail boots insulated with bear fur. Sturdy and reliable, they have numerous laces and straps that allow them to be fitted precisely to the foot.",
              "long_description":"Skin can be applied on: Heavy Chainmail Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151741,
              "sku":"60081",
              "name":"Royal Chainmail Brestplate (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/bc28f7db772a0969a00d832c4a94b7ee.png",
              "description":"Thick chain mail that reaches down to the knees assembled from rune-marked links. On the chest are leather pads with engraved ornaments. This armor is an item of rare durability and beauty. A fine piece of work indeed.",
              "long_description":"Skin can be applied on: Royal Chainmail Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151742,
              "sku":"60082",
              "name":"Royal Chainmail Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/45300b52191cbcecb42c94c9349be088.png",
              "description":"The lining of these bracers is bright crimson and made from uncommon, shimmering fabric probably acquired on a campaign. The bracer itself is made of tiny rings of tempered steel.",
              "long_description":"Skin can be applied on: Royal Chainmail Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151743,
              "sku":"60083",
              "name":"Royal Chainmail Gauntlets (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/747ac0067db63ccea8432219c609ca79.png",
              "description":"Well-made chain-mail gauntlets. They are lined with thick, fluffy fur and laced with red ribbon.",
              "long_description":"Skin can be applied on: Royal Chainmail Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151744,
              "sku":"60084",
              "name":"Royal Chainmail Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/033a2fb340bfee6c255581756de9fbaa.png",
              "description":"Leggings like a layer cake. They feature canvas with runes sewn into them, leather for durability, and fur for warmth. They are reinforced with a chain netting. Few spears can pierce leggings such as these.",
              "long_description":"Skin can be applied on: Royal Chainmail Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151745,
              "sku":"60085",
              "name":"Royal Chainmail Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e0ed8d3c8b5e5118474852d301679c8f.png",
              "description":"Sewn to fit the foot precisely, these chain-mail boots will serve their owner faithfully through many battles and campaigns. Bear fur coated with lard protects against the fiercest frost, and the talismans and runes sewn into the crimson lining protect against other manners of peril.",
              "long_description":"Skin can be applied on: Royal Chainmail Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151810,
              "sku":"60239",
              "name":"Royal Chainmail Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8a518ba4bde2e41caa58a722dd8624bc.png",
              "description":"A menacing horned helmet. A gilded half-mask and a long, thick aventail reaching all the way down to the warrior's chest are attached to the forged headpiece.",
              "long_description":"Skin can be applied on: Heavy Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151811,
              "sku":"60240",
              "name":"Royal Chainmail Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e0a1f4966bc025c5f69e894897ad8ebb.png",
              "description":"This long chainmail tunic is striking thanks to its abundance of detail. It is thickly woven and has a forged breastplate to which a fur cloak with a chainmail backing is attached with straps.",
              "long_description":"Skin can be applied on: Heavy Chainmail Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151812,
              "sku":"60241",
              "name":"Royal Chainmail Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/654a67a45cea611980287338e4f9432d.png",
              "description":"Chainmail bracers with leather lining and large plates covering the most vulnerable areas.",
              "long_description":"Skin can be applied on: Heavy Chainmail Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151813,
              "sku":"60242",
              "name":"Royal Chainmail Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/2bfcd7a733d24c66b0c76bcd94a1c979.png",
              "description":"Gauntlets made of the smallest rings. They are durable enough to protect the arm and thin enough not to interfere with the warrior's grip. The back of the hand is decorated with silver-plated plates.",
              "long_description":"Skin can be applied on: Heavy Chainmail Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151814,
              "sku":"60243",
              "name":"Royal Chainmail Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6542847f402e84794314abac9b6b9583.png",
              "description":"Pants made of strips of ring mail. They stand apart from most similar armor thanks to the unparalleled quality of the weaving and their forged greaves with patterns tooled along the edges.",
              "long_description":"Skin can be applied on: Heavy Chainmail Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151735,
              "sku":"600731",
              "name":"Heavy Chainmail Helm - Bear Trophy (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ed1a574987e6ec76fd2ffb524408291a.png",
              "description":"Inside the bear's head is a steel helm with a mask and a chain aventail covering the warrior's neck.",
              "long_description":"Skin can be applied on: Heavy Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":54000,
              "vc_amount_without_discount":60000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151734,
              "sku":"60071",
              "name":"Regular Chainmail Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/bc6bdea3f2e7253ab6e9e5a9f9925947.png",
              "description":"Canvas pants. They are sturdy, insulated, and have pockets for a pipe, flint and tinder, and other soldier's things.",
              "long_description":"Skin can be applied on: Regular Chainmail Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151733,
              "sku":"60070",
              "name":"Regular Chainmail Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/bb233e97d0867ebeaf51e5c8cda1acb1.png",
              "description":"Sailor's boots made of rough leather. Simple, durable, and double-stitched. A chain netting is concealed behind the opening to protect the calves.",
              "long_description":"Skin can be applied on: Regular Chainmail Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151636,
              "sku":"60066",
              "name":"Regular Chainmail Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a2e26fd2dc47728a0696125b3265fe4e.png",
              "description":"A sturdy, well-made Slavard helm. Simple, with no attempt at elegance, and with a think aventail lined with sackcloth to protect the ears and neck.",
              "long_description":"Skin can be applied on: Regular Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151637,
              "sku":"60073",
              "name":"Heavy Chainmail Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4119a51f1dd7f43b3beeaf7352a3ab3f.png",
              "description":"A sturdy steel skullcap decorated with ornamental script. The warrior's face is hidden behind steel defenses, and the nape of the neck is protected by an aventail.",
              "long_description":"Skin can be applied on: Heavy Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151638,
              "sku":"60080",
              "name":"Royal Chainmail Helm (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5e38c291054601627b6222358398f339.png",
              "description":"A gilded helm with a riveted nasel and a bear's head. A sign of a jarl or a field commander from his retinue.",
              "long_description":"Skin can be applied on: Royal Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":44100,
              "vc_amount_without_discount":49000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151687,
              "sku":"70065",
              "name":"Light Chainmail Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1803b09a7e22393d50d3310e6baa09f0.png",
              "description":"Simple armor that smells like resin and the distant, salty sea for some reason. Boots, helm, chain mail... every northerner has donned such simple, unpretentious armor at least once.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151688,
              "sku":"70072",
              "name":"Regular Chainmail Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e5c795ddafb94f4495cfc991f5fe0117.png",
              "description":"Armor marked with signs for those who are able to see them: short stripes of white paint on the collar and light blue ribbons diving between the rings. This is skillfully made armor that will serve you well.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151689,
              "sku":"70079",
              "name":"Heavy Chainmail Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5cd2984d722e929ee9870970c408115c.png",
              "description":"Armor like this – weighty, heavy, and precise in a un-Slavard kind of way – has always been an indispensable sign of the most daring and fearless warriors, swordsmen, and heroes. In the far north warriors such as these are known by name. Songs are sung of them, and stories are told of them. Armor like this is rarely encountered and is expensive, but the price is hardly the point.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151724,
              "sku":"60060",
              "name":"Light Chainmail Tunic (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/2cc389cf02ab6580bd6fe8acdd08c7cf.png",
              "description":"Short chain mail with no sleeves, woven in two layers. It is decorated with a simple, shapeless wolf hide collar.",
              "long_description":"Skin can be applied on: Light Chainmail Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151725,
              "sku":"60061",
              "name":"Light Chainmail Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/7561909a104deef6e1aa33b50d2cbb6c.png",
              "description":"These bracers are made of sturdy canvas that have been soaked in something.",
              "long_description":"Skin can be applied on: Light Chainmail Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151726,
              "sku":"60062",
              "name":"Light Chainmail Leggings (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/04a6e14653c20908cf032314f5d5e92e.png",
              "description":"Simple northern leggings – not too pretty, but sturdy, warm, and comfortable. The chain patches are sewn on with coarse thread protect the warrior's legs just below the knees.",
              "long_description":"Skin can be applied on: Light Chainmail Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151727,
              "sku":"60063",
              "name":"Light Chainmail Greaves (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/bd263c7c3e9101fbc5cab11422a5c83c.png",
              "description":"The northerners have never been particularly skilled shoemakers, and these boots are proof of that. The soles and the boots themselves are made of rough cow leather with fur on the inside. Soft straps have been sewn onto the shapeless bootlegs, allowing the boots to fit the foot snugly.",
              "long_description":"Skin can be applied on: Light Chainmail Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151728,
              "sku":"60064",
              "name":"Light Chainmail Gauntlets (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ebae6f2a0dc86c6ee506d22447b88481.png",
              "description":"Roughly made fingerless gauntlets made of thick, coarse leather.",
              "long_description":"Skin can be applied on: Light Chainmail Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151729,
              "sku":"600661",
              "name":"Regular Chainmail Helm - Wolf trophy (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1617fc32d70eb306123e903176b1e66e.png",
              "description":"The inside of this forged helm shines through the thin eye-holes of the wolf's head. A thin chain aventail is sewn to the fur to form a kind of lining.",
              "long_description":"Skin can be applied on: Regular Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":27000,
              "vc_amount_without_discount":30000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151730,
              "sku":"60067",
              "name":"Regular Chainmail Breastplate (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/eb501a85dc9c5a50118a621acccd3a1d.png",
              "description":"Chain mail made from large rings. Not especially thick, but surprisingly light and durable. A wolf-fur collar rests on the back, providing the wearer with additional protection and warmth.",
              "long_description":"Skin can be applied on: Regular Chainmail Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151731,
              "sku":"60068",
              "name":"Regular Chainmail Vambraces (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/30f36d3cde9ef5ddce617fd750abea4b.png",
              "description":"A chain-mail bracer with steel ribs and coarse leather lining.",
              "long_description":"Skin can be applied on: Regular Chainmail Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151732,
              "sku":"60069",
              "name":"Regular Chainmail Gauntlets (Slavard style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/41fb0906e496aa03db43a1d4511a488f.png",
              "description":"Insulated gauntlets made from tanned leather with straps hidden on the wrists.",
              "long_description":"Skin can be applied on: Regular Chainmail Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151815,
              "sku":"60244",
              "name":"Royal Chainmail Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d3b65101b08609a096bae309d79a4678.png",
              "description":"Boots made of strips of ring mail with steel tops. Made of calfskin, they are sturdy on the outside and soft and comfortable on the inside.",
              "long_description":"Skin can be applied on: Heavy Chainmail Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6731,
        "name":"Scale Skins",
        "virtual_items":[
           {
              "id":151632,
              "sku":"60031",
              "name":"Light Scale Helm (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/2ddda9d8aae400622786d00cf373f74c.png",
              "description":"This helm is thin, light, and completely lacking in durability. It is made from some kind of soft metal and has a leather lining. Leather flaps with irregular iron scales sewn onto them rest against the warrior's neck and shoulders.",
              "long_description":"Skin can be applied on: Light Scale Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151714,
              "sku":"60046",
              "name":"Heavy Scale Tunic (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/fe210cc733da85dc4f53f1aeb52ebbef.png",
              "description":"This tunic edged with wolf fur is surprisingly sturdy and heavy. The waves of thick steel scales aren't sewn on, but fastened to one another in an overlapping pattern – no spear or sword can get past them. The edges and sides are decorated with engraved leather pads.",
              "long_description":"Skin can be applied on: Heavy Scale Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151715,
              "sku":"60047",
              "name":"Heavy Scale Vambraces (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/013ec9df01d7c331999282a21294d749.png",
              "description":"Comfortable bracers made from two layers of tanned leather and reinforced with sturdily fastened iron feathers.",
              "long_description":"Skin can be applied on: Heavy Scale Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151716,
              "sku":"60048",
              "name":"Heavy Scale Gauntlets (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/972937d902d3d1a75f32a524f3e7f40a.png",
              "description":"Riveted leather gauntlets, a surprisingly fine job for steppe-dwellers. The fingers are protected with iron plates.",
              "long_description":"Skin can be applied on: Heavy Scale Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151717,
              "sku":"60049",
              "name":"Heavy Scale Leggings (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6cc9eba7909ff9e4a311d074d3766d9b.png",
              "description":"Comfortable, wide leggings made of skillfully treated leather with numerous faceted scales made from uncommonly good steel sewn onto them.",
              "long_description":"Skin can be applied on: Heavy Scale Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151718,
              "sku":"60050",
              "name":"Heavy Scale Greaves (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c48b785d229b33b31e0738a8f156afe1.png",
              "description":"Snub-nosed leather boots richly adorned with copper plates. Warm and soft, but surprisingly sturdy, with steel feathers sturdily sewn into the calf and instep.",
              "long_description":"Skin can be applied on: Heavy Scale Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151719,
              "sku":"60053",
              "name":"Royal Scale Brestplate (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a18fba82c2bcf5f12a55540b11f2409d.png",
              "description":"A very solid, sturdy, and painstakingly made peasant blouse covered with thick steel scales fastened in several rows. The leather is dyed bright red and adorned with patterns sewn with gold and red thread.",
              "long_description":"Skin can be applied on: Royal Scale Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151720,
              "sku":"60054",
              "name":"Royal Scale Vambraces (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4b6928c9ae7ab79100682defad097883.png",
              "description":"Solid leather bracers adorned with copper rivets with pictures of horses' heads on them.",
              "long_description":"Skin can be applied on: Royal Scale Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151721,
              "sku":"60055",
              "name":"Royal Scale Gauntlets (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1679306e9148d5c0840f65312e8cba90.png",
              "description":"Leather gauntlets dotted with rivets. Quite sturdy and solid in order to protect the arm against spears and swords. They are decorated with a complex tooled pattern.",
              "long_description":"Skin can be applied on: Royal Scale Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151722,
              "sku":"60056",
              "name":"Royal Scale Leggings (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f8afbba840fc55ee3ffddfce3d81c710.png",
              "description":"A black and red skirt-like article of clothing made of smooth silk covers sturdy leggings made of felt and leather topped with tiny steel scales.",
              "long_description":"Skin can be applied on: Royal Scale Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151723,
              "sku":"60057",
              "name":"Royal Scale Greaves (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/16ba9f400a2a6003e40b744e6729c00c.png",
              "description":"These boots are richly adorned with tiny gilded scales and sewn with dark red thread. They are lined with calfskin and some kind of short, thick fur.",
              "long_description":"Skin can be applied on: Royal Scale Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151804,
              "sku":"60232",
              "name":"Royal Scale Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/9bcfc2e8628f60357025024fea993b1f.png",
              "description":"Beneath the lining of this luxurious conical helmet hides a sturdy aventail framed with blackened steel plates. The warrior's face is protected by a mask of pure silver.",
              "long_description":"Skin can be applied on: Heavy Scale Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151805,
              "sku":"60233",
              "name":"Royal Scale Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/0c41c1f4d64404ed38fab9c5e2b701d7.png",
              "description":"This long-hemmed tunic is covered with hundreds of steel scales, and the pauldrons, sleeves, and hems are elegantly adorned with golden trim.",
              "long_description":"Skin can be applied on: Heavy Scale Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151806,
              "sku":"60234",
              "name":"Royal Scale Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/61a99878a94ad4522fd9e30ab2fabeb0.png",
              "description":"A wave of triangular scales streams down the shoulders of these bracers, ending in sleeves made of several thick plates linked by elegant gilded buckles.",
              "long_description":"Skin can be applied on: Heavy Scale Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151807,
              "sku":"60235",
              "name":"Royal Scale Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ffda0f87c52fd580a093b9cc3782c98e.png",
              "description":"These gauntlets are somehow ungainly, being made entirely from tiny plates of polished steel. On the inside is a silk lining tied to the edges of the gauntlets by thick golden trim.",
              "long_description":"Skin can be applied on: Heavy Scale Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151808,
              "sku":"60236",
              "name":"Royal Scale Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/209ce86755888cf7b9aed6f8a3b837ba.png",
              "description":"Soft pants made of ox leather covered with square steel plates as smooth as mirrors.",
              "long_description":"Skin can be applied on: Heavy Scale Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151713,
              "sku":"60043",
              "name":"Regular Scale Greaves (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/9a6ef61e4c8040558ac374332286412e.png",
              "description":"Durable and yet very soft boots with scuffed tips. Thick leather straps with iron scales are wrapped around them.",
              "long_description":"Skin can be applied on: Regular Scale Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151712,
              "sku":"60042",
              "name":"Regular Scale Leggings (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/10bb2c03c97829f914c0742a9dc27199.png",
              "description":"Felt leggings that end in leather bootlegs thickly covered with metal scales.",
              "long_description":"Skin can be applied on: Regular Scale Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151633,
              "sku":"60045",
              "name":"Heavy Scale Helm (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/298a9a40aae178dacb66ed9418b1bf0b.png",
              "description":"A pointed helm made of surprisingly decent iron. It is decorated with bronze plates and a tail fastened to the top.",
              "long_description":"Skin can be applied on: Heavy Scale Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151634,
              "sku":"60052",
              "name":"Royal Scale Helm (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/45f31bcbebaffbd4b33c6c18fd9cba7d.png",
              "description":"A pointed helmet decorated with various designs and a fire-red tail. It is covered with a thin layer of glossy lacquer, probably to protect the gilded depiction of galloping steeds, sabers, and great burial mounds drawn in a frame made of tongues of flame.",
              "long_description":"Skin can be applied on: Royal Scale Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":44100,
              "vc_amount_without_discount":49000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151648,
              "sku":"60038",
              "name":"Regular Scale Helm (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f9c500b339c5c3bf3eab3d011d127f60.png",
              "description":"A helm made from plates of soft iron of the lowest quality. The top of the helm is adorned with a horse's tail.",
              "long_description":"Skin can be applied on: Regular Scale Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151673,
              "sku":"70058",
              "name":"Royal Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e9897ecb4fc0318d40b8b3505586c62b.png",
              "description":"Armor like this – heavy, massive, and richly decorated, with gleaming gilt and the crimson color of fresh blood – isn't something most people can afford. With a little effort this armor could be traded for an entire herd of horses.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151683,
              "sku":"70037",
              "name":"Light Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/596f9b1004f62eaa9cec0f67d692affe.png",
              "description":"Light nomad armor consisting of a scale tunic, a pointed helm, bracers, greaves, and other clothing made from soft treated leather. Sometimes armor like this is decorated with beads, trim, or ribbons, but more often it's just coarse, dirty clothing covered in the dust of the steppe.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151684,
              "sku":"70044",
              "name":"Regular Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8ccd96b4932c5cdbbc8b0a151468e2c4.png",
              "description":"This sturdy armor marks its wearer's every step with a quiet steel rustle, as if to say, \"a great warrior is coming!\" The thick scales on the blouse and the horse's tail on the helm are the telltale signs of a Khoors commanding officer.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151685,
              "sku":"70051",
              "name":"Heavy Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5804486ac2fdf03c15b9ad73058a910b.png",
              "description":"Armor like this is a rarity even among the Khoors elite. Jealously guarded by its owner, this armor can last for decades until the colors lose their luster, the sweat-soaked fabric is worn through, or rust consumes the weak Khoors steel.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151704,
              "sku":"60032",
              "name":"Light Scale Tunic (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/cf718ff844323450282a1efeda22c7fe.png",
              "description":"A peasant's overcoat sewn with sinew and decorated with broad, sandy-colored trim. A single layer of scales has been sewn onto it.",
              "long_description":"Skin can be applied on: Light Scale Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151705,
              "sku":"60033",
              "name":"Light Scale Vambraces (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ba4e9666c2674f4ae8d24e80f848ef75.png",
              "description":"Ox leather bracers with iron scales sewn unevenly onto them.",
              "long_description":"Skin can be applied on: Light Scale Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151706,
              "sku":"60034",
              "name":"Light Scale Gauntlets (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/af12ba4577550f7285a25210ad1f19c8.png",
              "description":"Khoors leather gauntlets with no lining, slightly reinforced with thick pads on the back of the hands.",
              "long_description":"Skin can be applied on: Light Scale Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151707,
              "sku":"60035",
              "name":"Light Scale Leggings (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e41922db5bd378819e63aa0ac4e0c24a.png",
              "description":"Broad leggings made from thick felt and dyed brown, the color of the steppe. Scale greaves protect the legs just below the knees.",
              "long_description":"Skin can be applied on: Light Scale Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151708,
              "sku":"60036",
              "name":"Light Scale Greaves (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b9a85385f880a519394e9adb4cf8c02e.png",
              "description":"Boots sewn with sinew that have sturdy leather soles. Rope and scraps of old cloth have been wrapped around them for increased toughness.",
              "long_description":"Skin can be applied on: Light Scale Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151709,
              "sku":"60039",
              "name":"Regular Scale Tunic (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/0801b4a62cf77061e5c456c268d439e4.png",
              "description":"A thick tunic with a layer of leather over it. Even rows of long iron scales are sewn across it.",
              "long_description":"Skin can be applied on: Regular Scale Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151710,
              "sku":"60040",
              "name":"Regular Scale Vambraces (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/502ebb22167fdd1c2ce63750056bbed9.png",
              "description":"Scale bracers made of leather and sturdy sackcloth sewn with ox sinew.",
              "long_description":"Skin can be applied on: Regular Scale Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151711,
              "sku":"60041",
              "name":"Regular Scale Gauntlets (Khoors style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/461c5e4cd7153984714e793cbc5ec94a.png",
              "description":"These gauntlets are made from several layers of rough treated leather and stitched with 'coarse' thread along the edge. It's quite heavy – there are probably steel scales sewn in underneath the lining.",
              "long_description":"Skin can be applied on: Regular Scale Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151809,
              "sku":"60237",
              "name":"Royal Scale Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8323dae2521c9a543a6898b04f2fc0e4.png",
              "description":"These boots are comprised of forged steel boots and soft leather tops covered with rows of overlapping steel plates. Straps that help to fit the boot snugly to the foot are hidden under some of the plates.",
              "long_description":"Skin can be applied on: Heavy Scale Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6729,
        "name":"Plate Skins",
        "virtual_items":[
           {
              "id":151628,
              "sku":"60003",
              "name":"Full Plate Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/784008717945bf0f81c27e864d003a0c.png",
              "description":"A Gottlung plate helm with a solid visor. The blacksmith's brand is illegible.",
              "long_description":"Skin can be applied on: Full Plate Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151677,
              "sku":"70009",
              "name":"Full Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/53ed8fff15a4a595755978e6cf14bab7.png",
              "description":"Full plate armor. Precise and without any extras. A brand with three arrows is carved on the inside of the breastplate.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151672,
              "sku":"70030",
              "name":"Royal Full Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a4fc4af4aedd022249b812e6fee06f73.png",
              "description":"No cabe duda de que es una de las armaduras más lujosas que se han visto. Los grabados y los patrones labrados se funden a la perfección con el baño dorado que cubre por completo la armadura.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151679,
              "sku":"70016",
              "name":"Half Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4f4898ec7425aece4f8b6ee4d80e1c67.png",
              "description":"The armor of the black infantry, scratched and covered with blotches of caked-on mud. Armor like this usually makes its way to city markets from a battlefield, and it's tough to say how many wearers it has survived.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151686,
              "sku":"70023",
              "name":"Iron Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/86b6702a97d8dbfc8ecb526f48e4cbf0.png",
              "description":"This armor is good, solid, and reasonably elegant, just like everything made by the Gottlungs. Numerous straps and clasps allow this armor to be fitted to any body type.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151649,
              "sku":"60004",
              "name":"Full Plate Breastplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8a94dd2de9e0efeeb6c9c3212c0a96d2.png",
              "description":"A shiny, solid breastplate with a chain gorget and massive pauldrons.",
              "long_description":"Skin can be applied on: Full Plate Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151650,
              "sku":"60005",
              "name":"Full Plate Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/267232a37923b8b77b600535319898c3.png",
              "description":"A pair of heavy plate bracers. Black straps of tawed leather are hidden beneath a layer of plates.",
              "long_description":"Skin can be applied on: Full Plate Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151651,
              "sku":"60006",
              "name":"Full Plate Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c2fe4888eefea6520d0760cffb1141fa.png",
              "description":"Plate gauntlets assembled from tiny plates of blackened metal. A Gottlung pattern has been carved into the cuffs, which are polished to a shine.",
              "long_description":"Skin can be applied on: Full Plate Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151652,
              "sku":"60007",
              "name":"Full Plate Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/47d7237fd76e9ab765e43fc80bf49952.png",
              "description":"Blackened plate greaves on a solid chain-mail base.",
              "long_description":"Skin can be applied on: Full Plate Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151653,
              "sku":"60008",
              "name":"Full Plate Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/81bf580c342b26d6c369dcc9fe0c4018.png",
              "description":"A pair of leather boots covered with blackened steel plates that protect the foot on all sides. Very durable and, believe it or not, comfortable footwear.",
              "long_description":"Skin can be applied on: Full Plate Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151629,
              "sku":"60010",
              "name":"Half Plate Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c2ec08b948ba84de5c9881183bb4a637.png",
              "description":"A flat-topped Gottlung helm with quilted lining.",
              "long_description":"Skin can be applied on: Half Plate Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":13500,
              "vc_amount_without_discount":15000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151654,
              "sku":"60011",
              "name":"Half Plate Breastplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/bf51bc7fd434ec6251fe6a553bab0777.png",
              "description":"A tempered breastplate that retains traces of numerous blows. The metal is solid, light, and has been very lightly blackened — a telltale sign of Gottlung armor.",
              "long_description":"Skin can be applied on: Half Plate Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151655,
              "sku":"60012",
              "name":"Half Plate Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f8780efb59fdf0ec0d307dbfe211f03d.png",
              "description":"Well-worn bracers made of blackened metal.",
              "long_description":"Skin can be applied on: Half Plate Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151656,
              "sku":"60013",
              "name":"Half Plate Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b7196d8e2ed233183c05db768223c8b0.png",
              "description":"Gottlung-made plate gauntlets. The overlapping thick plates fastened on a coarse leather base protect the arm against blows.",
              "long_description":"Skin can be applied on: Half Plate Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151657,
              "sku":"60014",
              "name":"Half Plate Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/cf598737a60d9909bf6bac8645b81829.png",
              "description":"Solid quilted leggings with heavy, battle-scarred greaves.",
              "long_description":"Skin can be applied on: Half Plate Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151678,
              "sku":"60015",
              "name":"Half Plate Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a57f7965024d4fba7405633422cb868f.png",
              "description":"Infantry boots with a layer of plate mail — inexpensive, but well made. The ankles and heels are covered with a chain netting.",
              "long_description":"Skin can be applied on: Half Plate Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151630,
              "sku":"60017",
              "name":"Iron Plate Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c1f4c82de11b0ab2f5a761ac3298b4ed.png",
              "description":"A flat Gottlung helm. It's a little worn and has a barely-noticeable reddish tint to its numerous nicks.",
              "long_description":"Skin can be applied on: Iron Plate Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":10800,
              "vc_amount_without_discount":12000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151680,
              "sku":"60018",
              "name":"Iron Plate Breastplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/7572352c7d77343c57580a01d52a750b.png",
              "description":"A thin chain-mail shirt with pauldrons, a forged gorget, and iron plates protecting the stomach and sides. Roughly, but competently made.",
              "long_description":"Skin can be applied on: Iron Plate Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151696,
              "sku":"60019",
              "name":"Iron Plate Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/15960dc28a14af034a7bb367b54cb431.png",
              "description":"Well-worn leather bracers with iron patches fastened onto them.",
              "long_description":"Skin can be applied on: Iron Plate Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151681,
              "sku":"60020",
              "name":"Iron Plate Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/bfd13f0022bcdd388948780edc6f5de5.png",
              "description":"Coarse gauntlets sewn together from patches of leftover bull hide. On the back of the hand is an iron plate held on with rivets.",
              "long_description":"Skin can be applied on: Iron Plate Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151697,
              "sku":"60021",
              "name":"Iron Plate Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a432b4225c4472bfe6166694dbbecf92.png",
              "description":"Quilted leggings with round iron greaves bound by straps.",
              "long_description":"Skin can be applied on: Iron Plate Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151698,
              "sku":"60022",
              "name":"Iron Plate Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/27fb326cc1c1e2a54f58e769d74d7f39.png",
              "description":"These shoes are soft, durable, and have steel soles.",
              "long_description":"Skin can be applied on: Iron Plate Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":8100,
              "vc_amount_without_discount":9000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151699,
              "sku":"60025",
              "name":"Royal Full Plate Breastplate (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e3d07400aa01e2a0d828f130b77b65e9.png",
              "description":"A burnished breastplate with the visage of Amate finely engraved upon it. Golden rays emanate from her countenance.",
              "long_description":"Skin can be applied on: Royal Full Plate Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151700,
              "sku":"60026",
              "name":"Royal Full Plate Vambraces (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/53d0c372ddeb4987cfb44626d44c4d2c.png",
              "description":"Blackened bracers covered with a tooled Gottlung pattern and words of prayer engraved upon it.",
              "long_description":"Skin can be applied on: Royal Full Plate Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151701,
              "sku":"60027",
              "name":"Royal Full Plate Gauntlets (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/7bcc3d0a29768b6ac66ab7462265ebcb.png",
              "description":"Luxurious gauntlets lined with red silk. They are covered with steel plates with a thin layer of gilt.",
              "long_description":"Skin can be applied on: Royal Full Plate Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151702,
              "sku":"60028",
              "name":"Royal Full Plate Leggings (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c5e9f1d532f20771dafedfed792777a5.png",
              "description":"Knight's greaves with gilded knee protectors. They are assembled from numerous pieces riveted together. Truly fine work.",
              "long_description":"Skin can be applied on: Royal Full Plate Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151703,
              "sku":"60029",
              "name":"Royal Full Plate Greaves (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a96416fb4a9e99407961ba7b22534ff4.png",
              "description":"Plate boots fitted precisely to the foot. Burnished, gilded plates with an engraving are fastened onto a calfskin base.",
              "long_description":"Skin can be applied on: Royal Full Plate Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151798,
              "sku":"60225",
              "name":"Royal Full Plate Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5254c0897f77e626ba9970e25ec4fbed.png",
              "description":"A bascinet made from excellent Gottlung steel. The visor with narrow eye-slits has been polished to a fine sheen. The helmet itself is framed in a barely-visible leaf ornament and decorated with stripes of black and white lacquer.",
              "long_description":"Skin can be applied on: Full Plate Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151799,
              "sku":"60226",
              "name":"Royal Full Plate Breastplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/aa59e6ede75edc8ff988087732e9134b.png",
              "description":"A luxurious forged cuirass with a gorget and shoulders made from small polished plates. The breastplate is adorned with black and white stripes, and words of prayer shine upon it with the dull glimmer of gilt.",
              "long_description":"Skin can be applied on: Full Plate Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151800,
              "sku":"60227",
              "name":"Royal Full Plate Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5d5b7b0ec130a6d1d2d4d9bc72efdebb.png",
              "description":"A plate bracer adorned with black and white stripes. It covers the elbow and forearm well but is still light and mobile. It is decorated with monograms drawn in black lacquer and lined with soft leather on the inside.",
              "long_description":"Skin can be applied on: Full Plate Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151801,
              "sku":"60228",
              "name":"Royal Full Plate Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a15eec93fd3ce6d7e8132372677f35cf.png",
              "description":"These warm, comfortable gauntlets are made from soft leather treated in a special manner. The inside of the gauntlets is lined with overlapping plates of tempered steel that can even withstand a blow from a poleaxe.",
              "long_description":"Skin can be applied on: Full Plate Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151802,
              "sku":"60229",
              "name":"Royal Full Plate Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c33f9735c45917d62b09b67af8a0a690.png",
              "description":"Chainmail pants made of tiny rings of blackened steel and ending in steel greaves. The polished metal is decorated with an engraving, and the plates on the greaves are joined with gilded rivets.",
              "long_description":"Skin can be applied on: Full Plate Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151631,
              "sku":"60024",
              "name":"Royal Full Plate Helm (Gottlung style)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1aff6667dec337eb69fb85662c4458c5.png",
              "description":"A shiny plate helm with a thin layer of gilt and a barely noticeable engraving on the visor.",
              "long_description":"Skin can be applied on: Royal Full Plate Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":44100,
              "vc_amount_without_discount":49000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151803,
              "sku":"60230",
              "name":"Royal Full Plate Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/77bb54431bb04dd37a971abf0014ced4.png",
              "description":"Knight's boots made of steel plates. Despite their weight, they are rather comfortable and do not hamper the wearer's movements.",
              "long_description":"Skin can be applied on: Full Plate Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6741,
        "name":"Royal Style",
        "virtual_items":[
           {
              "id":151816,
              "sku":"60246",
              "name":"Royal Leather Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/91e4ca3bba181916073f157dff2a6ffd.png",
              "description":"A pointed helmet reminiscent of a Khoors tent. The top part is forged, and the helmet itself is made of several layers of thick studded leather and outfitted with a steel nose-guard.",
              "long_description":"Skin can be applied on: Heavy Leather Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151817,
              "sku":"60247",
              "name":"Royal Leather Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ccace5b930ecaecf376477169db6d8b1.png",
              "description":"A tastefully made and decorated calfskin overcoat with a broad crimson belt and a felt doublet that can absorb hacking blows. It is reinforced with studded-leather and steel plates.",
              "long_description":"Skin can be applied on: Heavy Leather Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151818,
              "sku":"60248",
              "name":"Royal Leather Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/77ab75bffcf5fd1cde6c5ece2a758909.png",
              "description":"A pair of leather bracers made of pieces of boiled leather riveted together. They are stitched for greater sturdiness. The elbows are protected with steel plates.",
              "long_description":"Skin can be applied on: Heavy Leather Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151819,
              "sku":"60249",
              "name":"Royal Leather Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4cff065c9abc74b870478b2624f145cb.png",
              "description":"Leather gauntlets with soft cuffs. They are lined with black silk sewn with silvery thread. They fit the hand... well, like a glove.",
              "long_description":"Skin can be applied on: Heavy Leather Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151820,
              "sku":"60250",
              "name":"Royal Leather Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/fc58c642919a60d980dfaee6b7ec8384.png",
              "description":"These leather greaves are made of layers of lacquered leather riveted together in an overlapping pattern. The abundance of rivets, tooled decorations, and patterned trim make them uniquely elegant and beautiful items.",
              "long_description":"Skin can be applied on: Heavy Leather Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151821,
              "sku":"60251",
              "name":"Royal Leather Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ad69b05eb3f62dc00cdd70319698af9e.png",
              "description":"Short dark-red morocco boots. They appear deceptively light, but penetrating them isn't easy – their tops are reinforced with two layers of studded leather that reliably protect the warrior's legs.",
              "long_description":"Skin can be applied on: Heavy Leather Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151845,
              "sku":"70252",
              "name":"Royal Leather Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d9af7499891c2e50b1727c601844297e.png",
              "description":"Gold trim and scales of crimson leather give this armor an appearance that is luxurious and menacing at the same time. Suits of armor such as this can be counted on two hands, and the names of their owners are known to every inhabitant of the lands of the former Empire.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151822,
              "sku":"60253",
              "name":"Royal Padded Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6ebd2f1eefc78429b44a0894f9e96291.png",
              "description":"A skullcap made of tempered steel with a quilted aventail. Double-stitched sturdy silk thread combines two layers of soft leather lined with horsehair and a fine chainmail netting that cannot be seen from the side.",
              "long_description":"Skin can be applied on: Heavy Padded Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151823,
              "sku":"60254",
              "name":"Royal Padded Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/610c223d0cd2d14edae4e881d9a8e20f.png",
              "description":"A light quilted jacket that fits the torso perfectly and is carefully conceived – behind the rabbit-fur lining is a layer of sticky boiled leather, and the most vulnerable areas are protected by thin steel plates.",
              "long_description":"Skin can be applied on: Heavy Padded Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151824,
              "sku":"60255",
              "name":"Royal Padded Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/01dd4ab7feeb58da07214791b8bd7671.png",
              "description":"Magnificently crafted quilted bracers adorned with stripes of blue-black leather.",
              "long_description":"Skin can be applied on: Heavy Padded Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151825,
              "sku":"60256",
              "name":"Royal Padded Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/730146995ca5bb0cdbe3ca8f291df769.png",
              "description":"Gauntlets made from several layers of coarse linen lined with pieces of tanned leather. Sturdy enough to stop a spear or soften a blow from a club.",
              "long_description":"Skin can be applied on: Heavy Padded Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151826,
              "sku":"60257",
              "name":"Royal Padded Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5339086abdaca4bdae06e25a055b1322.png",
              "description":"Quilted greaves. Strips of yarn, horsehair, and some chainmail netting can be felt through the dark-blue fabric. The greaves nevertheless provide excellent protection.",
              "long_description":"Skin can be applied on: Heavy Padded Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151827,
              "sku":"60258",
              "name":"Royal Padded Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1a0851c93ca5293ac2eab0a3c500c7ad.png",
              "description":"Boots made from thin leather covered with rhombuses of quilted linen. They would be weightless if not for the steel plates sewn inside of them to protect the calf and instep.",
              "long_description":"Skin can be applied on: Heavy Padded Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151846,
              "sku":"70259",
              "name":"Royal Padded Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/73aed97b179a05a4eb089bf5e738ad74.png",
              "description":"A suit of quilted armor that is as intimidating as it is practical. It looks simple, but expensive — perfect seams, attention to detail, chainmail stripes and plates hidden in the lining and capable of defending against even the strongest blows.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151810,
              "sku":"60239",
              "name":"Royal Chainmail Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8a518ba4bde2e41caa58a722dd8624bc.png",
              "description":"A menacing horned helmet. A gilded half-mask and a long, thick aventail reaching all the way down to the warrior's chest are attached to the forged headpiece.",
              "long_description":"Skin can be applied on: Heavy Chainmail Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151811,
              "sku":"60240",
              "name":"Royal Chainmail Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e0a1f4966bc025c5f69e894897ad8ebb.png",
              "description":"This long chainmail tunic is striking thanks to its abundance of detail. It is thickly woven and has a forged breastplate to which a fur cloak with a chainmail backing is attached with straps.",
              "long_description":"Skin can be applied on: Heavy Chainmail Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151812,
              "sku":"60241",
              "name":"Royal Chainmail Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/654a67a45cea611980287338e4f9432d.png",
              "description":"Chainmail bracers with leather lining and large plates covering the most vulnerable areas.",
              "long_description":"Skin can be applied on: Heavy Chainmail Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151813,
              "sku":"60242",
              "name":"Royal Chainmail Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/2bfcd7a733d24c66b0c76bcd94a1c979.png",
              "description":"Gauntlets made of the smallest rings. They are durable enough to protect the arm and thin enough not to interfere with the warrior's grip. The back of the hand is decorated with silver-plated plates.",
              "long_description":"Skin can be applied on: Heavy Chainmail Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151814,
              "sku":"60243",
              "name":"Royal Chainmail Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/6542847f402e84794314abac9b6b9583.png",
              "description":"Pants made of strips of ring mail. They stand apart from most similar armor thanks to the unparalleled quality of the weaving and their forged greaves with patterns tooled along the edges.",
              "long_description":"Skin can be applied on: Heavy Chainmail Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151815,
              "sku":"60244",
              "name":"Royal Chainmail Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d3b65101b08609a096bae309d79a4678.png",
              "description":"Boots made of strips of ring mail with steel tops. Made of calfskin, they are sturdy on the outside and soft and comfortable on the inside.",
              "long_description":"Skin can be applied on: Heavy Chainmail Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151844,
              "sku":"70245",
              "name":"Royal Chainmail Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/99538446941372be56a6f6e5a195465f.png",
              "description":"Full suits of ring mail of such masterful quality are extremely rare. This suit of armor might not be particularly fancy, but the intoxicating care and skill that went into its weaving is what makes it one of the finest in the world.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151804,
              "sku":"60232",
              "name":"Royal Scale Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/9bcfc2e8628f60357025024fea993b1f.png",
              "description":"Beneath the lining of this luxurious conical helmet hides a sturdy aventail framed with blackened steel plates. The warrior's face is protected by a mask of pure silver.",
              "long_description":"Skin can be applied on: Heavy Scale Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151805,
              "sku":"60233",
              "name":"Royal Scale Brestplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/0c41c1f4d64404ed38fab9c5e2b701d7.png",
              "description":"This long-hemmed tunic is covered with hundreds of steel scales, and the pauldrons, sleeves, and hems are elegantly adorned with golden trim.",
              "long_description":"Skin can be applied on: Heavy Scale Tunic\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151806,
              "sku":"60234",
              "name":"Royal Scale Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/61a99878a94ad4522fd9e30ab2fabeb0.png",
              "description":"A wave of triangular scales streams down the shoulders of these bracers, ending in sleeves made of several thick plates linked by elegant gilded buckles.",
              "long_description":"Skin can be applied on: Heavy Scale Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151807,
              "sku":"60235",
              "name":"Royal Scale Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ffda0f87c52fd580a093b9cc3782c98e.png",
              "description":"These gauntlets are somehow ungainly, being made entirely from tiny plates of polished steel. On the inside is a silk lining tied to the edges of the gauntlets by thick golden trim.",
              "long_description":"Skin can be applied on: Heavy Scale Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151808,
              "sku":"60236",
              "name":"Royal Scale Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/209ce86755888cf7b9aed6f8a3b837ba.png",
              "description":"Soft pants made of ox leather covered with square steel plates as smooth as mirrors.",
              "long_description":"Skin can be applied on: Heavy Scale Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151809,
              "sku":"60237",
              "name":"Royal Scale Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8323dae2521c9a543a6898b04f2fc0e4.png",
              "description":"These boots are comprised of forged steel boots and soft leather tops covered with rows of overlapping steel plates. Straps that help to fit the boot snugly to the foot are hidden under some of the plates.",
              "long_description":"Skin can be applied on: Heavy Scale Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151843,
              "sku":"70238",
              "name":"Royal Scale Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/7482ad97813ba4f623bf9cb4ba3c11b3.png",
              "description":"This is a striking suit of armor – waves of streaming steel, silver, morocco straps, and gold trim. It is a thing for very rich and powerful men, for the few whose names are repeated with fear and respect.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151798,
              "sku":"60225",
              "name":"Royal Full Plate Helm (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5254c0897f77e626ba9970e25ec4fbed.png",
              "description":"A bascinet made from excellent Gottlung steel. The visor with narrow eye-slits has been polished to a fine sheen. The helmet itself is framed in a barely-visible leaf ornament and decorated with stripes of black and white lacquer.",
              "long_description":"Skin can be applied on: Full Plate Helm\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151799,
              "sku":"60226",
              "name":"Royal Full Plate Breastplate (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/aa59e6ede75edc8ff988087732e9134b.png",
              "description":"A luxurious forged cuirass with a gorget and shoulders made from small polished plates. The breastplate is adorned with black and white stripes, and words of prayer shine upon it with the dull glimmer of gilt.",
              "long_description":"Skin can be applied on: Full Plate Breastplate\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151800,
              "sku":"60227",
              "name":"Royal Full Plate Vambraces (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5d5b7b0ec130a6d1d2d4d9bc72efdebb.png",
              "description":"A plate bracer adorned with black and white stripes. It covers the elbow and forearm well but is still light and mobile. It is decorated with monograms drawn in black lacquer and lined with soft leather on the inside.",
              "long_description":"Skin can be applied on: Full Plate Vambraces\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151801,
              "sku":"60228",
              "name":"Royal Full Plate Gauntlets (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a15eec93fd3ce6d7e8132372677f35cf.png",
              "description":"These warm, comfortable gauntlets are made from soft leather treated in a special manner. The inside of the gauntlets is lined with overlapping plates of tempered steel that can even withstand a blow from a poleaxe.",
              "long_description":"Skin can be applied on: Full Plate Gauntlets\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151802,
              "sku":"60229",
              "name":"Royal Full Plate Leggings (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c33f9735c45917d62b09b67af8a0a690.png",
              "description":"Chainmail pants made of tiny rings of blackened steel and ending in steel greaves. The polished metal is decorated with an engraving, and the plates on the greaves are joined with gilded rivets.",
              "long_description":"Skin can be applied on: Full Plate Leggings\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151803,
              "sku":"60230",
              "name":"Royal Full Plate Greaves (Default)",
              "image_url":"//cdn.xsolla.net/img/misc/images/77bb54431bb04dd37a971abf0014ced4.png",
              "description":"Knight's boots made of steel plates. Despite their weight, they are rather comfortable and do not hamper the wearer's movements.",
              "long_description":"Skin can be applied on: Full Plate Greaves\r",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":26100,
              "vc_amount_without_discount":29000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151842,
              "sku":"70231",
              "name":"Royal Full Plate Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/832af9ec329fbb712b9a97acd37728cf.png",
              "description":"A warrior in this armor senses greatness, might, and the significant labor and talent poured into it by an unknown master craftsman. From the side the armor looks almost monolithic – the painstakingly crafted steel plates on the helmet and breastplate are seamlessly connected. A thin black engraving of leaves and outlandish beasts wends its way around the armor from head to toe.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     },
     {
        "id":6738,
        "name":"Skin Packs",
        "virtual_items":[
           {
              "id":151690,
              "sku":"70093",
              "name":"Novice Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b1fb5f50d1b964cfff0b607d958205de.png",
              "description":"Unpretentious armor for spearmen and bowmen. It provides little defense, but does not restrict the wearer's movements.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151691,
              "sku":"70100",
              "name":"Regular Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1457cbd2fc01ff3f41630a80ab02f9bd.png",
              "description":"Ordinary armor of the type worn by poor mercenaries, armed travelers, and highwaymen. Easy to fix and reasonably priced.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151692,
              "sku":"70107",
              "name":"Heavy Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/fc5aa5a6206a843d9d0f63b4120b3709.png",
              "description":"Leather armor like this was encountered quite rarely in the good old days. It was typically worn by petty noblemen and mercenaries with connections. Nowadays anyone with enough gold can get their hands on a similar suit.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151675,
              "sku":"70114",
              "name":"Royal Leather Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d7506e338a0911bd3357bb0d7863a71e.png",
              "description":"Armor of this quality is rare indeed. Behind every suit of armor such as this is the story of a strong, distinguished warrior. Few are worthy to wear it.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151686,
              "sku":"70023",
              "name":"Iron Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/86b6702a97d8dbfc8ecb526f48e4cbf0.png",
              "description":"This armor is good, solid, and reasonably elegant, just like everything made by the Gottlungs. Numerous straps and clasps allow this armor to be fitted to any body type.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151679,
              "sku":"70016",
              "name":"Half Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/4f4898ec7425aece4f8b6ee4d80e1c67.png",
              "description":"The armor of the black infantry, scratched and covered with blotches of caked-on mud. Armor like this usually makes its way to city markets from a battlefield, and it's tough to say how many wearers it has survived.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151677,
              "sku":"70009",
              "name":"Full Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/53ed8fff15a4a595755978e6cf14bab7.png",
              "description":"Full plate armor. Precise and without any extras. A brand with three arrows is carved on the inside of the breastplate.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151672,
              "sku":"70030",
              "name":"Royal Full Plate Set (Gottlung style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/a4fc4af4aedd022249b812e6fee06f73.png",
              "description":"No cabe duda de que es una de las armaduras más lujosas que se han visto. Los grabados y los patrones labrados se funden a la perfección con el baño dorado que cubre por completo la armadura.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151693,
              "sku":"70121",
              "name":"Novice Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/ac03a2c80d59b43687c4a81eb5d09a1b.png",
              "description":"Rough, dirty rags that might protect against arrows or maybe clubs... if that.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151694,
              "sku":"70128",
              "name":"Regular Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/b2bfa5c628e3b489c0041c77e0b99f7c.png",
              "description":"Quilted armor insulated with fur and reinforced here and there with strips and pieces of leather. It isn't much to look at, but it's good at stopping arrows and absorbing club blows. It is solidly made from coarse fabric – not every sword can cut through quilted armor like this in a single blow.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151695,
              "sku":"70135",
              "name":"Heavy Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/c5d8f79212a14548f82d76091a245003.png",
              "description":"This suit of armor is light and preserves warmth for a long time. It is also soft and sturdy. Swords and axes basically get stuck in it with each blow.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151676,
              "sku":"70142",
              "name":"Royal Padded Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/497d64b0eb530cafcee3e912b665d552.png",
              "description":"A popular kind of armor among lightly armed northern warriors. The boar's head is a symbol of rage, and the crimson color is also a symbol, although everyone understands it in their way.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151687,
              "sku":"70065",
              "name":"Light Chainmail Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/1803b09a7e22393d50d3310e6baa09f0.png",
              "description":"Simple armor that smells like resin and the distant, salty sea for some reason. Boots, helm, chain mail... every northerner has donned such simple, unpretentious armor at least once.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151688,
              "sku":"70072",
              "name":"Regular Chainmail Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e5c795ddafb94f4495cfc991f5fe0117.png",
              "description":"Armor marked with signs for those who are able to see them: short stripes of white paint on the collar and light blue ribbons diving between the rings. This is skillfully made armor that will serve you well.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151689,
              "sku":"70079",
              "name":"Heavy Chainmail Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5cd2984d722e929ee9870970c408115c.png",
              "description":"Armor like this – weighty, heavy, and precise in a un-Slavard kind of way – has always been an indispensable sign of the most daring and fearless warriors, swordsmen, and heroes. In the far north warriors such as these are known by name. Songs are sung of them, and stories are told of them. Armor like this is rarely encountered and is expensive, but the price is hardly the point.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151674,
              "sku":"70086",
              "name":"Royal Chainmail Set (Slavard style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/f911ce7eae3800218ec7851c5302b004.png",
              "description":"Blood-red straps and the occasional glimmer of gold – this is no mere soldier's armor. It is a symbol of authority, power, and strength of will. Steel and fabric from distant shores, runes on the doublet... this is special armor indeed.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151683,
              "sku":"70037",
              "name":"Light Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/596f9b1004f62eaa9cec0f67d692affe.png",
              "description":"Light nomad armor consisting of a scale tunic, a pointed helm, bracers, greaves, and other clothing made from soft treated leather. Sometimes armor like this is decorated with beads, trim, or ribbons, but more often it's just coarse, dirty clothing covered in the dust of the steppe.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":35100,
              "vc_amount_without_discount":39000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151684,
              "sku":"70044",
              "name":"Regular Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/8ccd96b4932c5cdbbc8b0a151468e2c4.png",
              "description":"This sturdy armor marks its wearer's every step with a quiet steel rustle, as if to say, \"a great warrior is coming!\" The thick scales on the blouse and the horse's tail on the helm are the telltale signs of a Khoors commanding officer.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":53100,
              "vc_amount_without_discount":59000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151685,
              "sku":"70051",
              "name":"Heavy Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/5804486ac2fdf03c15b9ad73058a910b.png",
              "description":"Armor like this is a rarity even among the Khoors elite. Jealously guarded by its owner, this armor can last for decades until the colors lose their luster, the sweat-soaked fabric is worn through, or rust consumes the weak Khoors steel.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151673,
              "sku":"70058",
              "name":"Royal Scale Set (Khoors style) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/e9897ecb4fc0318d40b8b3505586c62b.png",
              "description":"Armor like this – heavy, massive, and richly decorated, with gleaming gilt and the crimson color of fresh blood – isn't something most people can afford. With a little effort this armor could be traded for an entire herd of horses.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":179100,
              "vc_amount_without_discount":199000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151845,
              "sku":"70252",
              "name":"Royal Leather Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/d9af7499891c2e50b1727c601844297e.png",
              "description":"Gold trim and scales of crimson leather give this armor an appearance that is luxurious and menacing at the same time. Suits of armor such as this can be counted on two hands, and the names of their owners are known to every inhabitant of the lands of the former Empire.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151846,
              "sku":"70259",
              "name":"Royal Padded Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/73aed97b179a05a4eb089bf5e738ad74.png",
              "description":"A suit of quilted armor that is as intimidating as it is practical. It looks simple, but expensive — perfect seams, attention to detail, chainmail stripes and plates hidden in the lining and capable of defending against even the strongest blows.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151844,
              "sku":"70245",
              "name":"Royal Chainmail Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/99538446941372be56a6f6e5a195465f.png",
              "description":"Full suits of ring mail of such masterful quality are extremely rare. This suit of armor might not be particularly fancy, but the intoxicating care and skill that went into its weaving is what makes it one of the finest in the world.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151843,
              "sku":"70238",
              "name":"Royal Scale Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/7482ad97813ba4f623bf9cb4ba3c11b3.png",
              "description":"This is a striking suit of armor – waves of streaming steel, silver, morocco straps, and gold trim. It is a thing for very rich and powerful men, for the few whose names are repeated with fear and respect.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           },
           {
              "id":151842,
              "sku":"70231",
              "name":"Royal Full Plate Set (Default) (6 pcs)",
              "image_url":"//cdn.xsolla.net/img/misc/images/832af9ec329fbb712b9a97acd37728cf.png",
              "description":"A warrior in this armor senses greatness, might, and the significant labor and talent poured into it by an unknown master craftsman. From the side the armor looks almost monolithic – the painstakingly crafted steel plates on the helmet and breastplate are seamlessly connected. A thin black engraving of leaves and outlandish beasts wends its way around the armor from head to toe.",
              "long_description":"",
              "currency":"USD",
              "amount":0,
              "amount_without_discount":0,
              "vc_amount":125100,
              "vc_amount_without_discount":139000,
              "bonus_virtual_currency":[

              ],
              "bonus_virtual_items":[

              ],
              "label":null,
              "offer_label":"",
              "advertisement_type":null,
              "quantity_limit":0,
              "is_favorite":0,
              "unsatisfied_user_attributes":[

              ],
              "unsatisfied_inventory_conditions":[

              ],
              "try_it_url":null,
              "promotion_end_time":null,
              "discount_percent":false,
              "vc_discount_percent":10
           }
        ]
     }
  ],
  "api":{
     "ver":"1.0.1"
  }
}