
var isMobile = false;
function defineMobile() {
    if( $('#ismobile').css('display')=='none') {
        isMobile = true;
    } else {
      isMobile = false;
    }
};
defineMobile();
$( window ).resize(function() {
  defineMobile();
});


var lightbox = {
  width: '740px',
  height: '625px',
  spinner: 'round',
  spinnerColor: '#fff',
  // contentBackground: "rgba(0,0,0,0.2)"
  contentBackground: '#000',
};

var access_data = {
    "custom_parameters": {
      "size": "S"
    },
    "user":{
        "id":{
            "value": "john_carter",
            "hidden": true
        },
        "name":{
            "value": "John Carter"
        },
        "country":{
            // "value": languageCurrency[local]['country'],
            "allow_modify": true
        }
    },
    "settings":{
      "project_id": 22877,
      "shipping_enabled": false,
        "ui": {
          "size": "medium",
          "theme": "dark"
        }
        // "language": local,
        // "currency": languageCurrency[local]['currency']
    },
    "purchase":{
        "pin_codes":{
            "codes": [
              {"digital_content": "ps-pp_digital_download_common"}
            ]
        }
    }
};




function xsolla_widget_simple(typ, sku, idBeginning, size, shipping_enabled) {
  // var local_simple = 'en';
  size = size || 'S';
  shipping_enabled = shipping_enabled || false;


  var access_data_simple = access_data;
  access_data_simple.purchase.pin_codes.codes[0].digital_content = sku;
  access_data_simple.custom_parameters.size = size;
  access_data_simple.settings.shipping_enabled = shipping_enabled;

  var options_simple = {
      access_data: access_data_simple,
      template: 'simple',
      lightbox: lightbox,
      // target_element: idBeginning + sku,
  };

  if (typ === 'button') {
    options_simple['target_element'] = idBeginning + sku;
    XPay2PlayWidget.create(options_simple);
  }
  if (typ === 'launch') {
    options_simple['target_element'] = '#ps-tshirt';
    // XPay2PlayWidget.create(options_simple);
    XPayStationWidget.init(options_simple);
    XPayStationWidget.open();
  }

}


function mikePrice(price) {
  if (price.split('.')[1] === '00') {
    price = price.split('.')[0];
  }
  var decimals = '';
  if (price.split('.').length && price.split('.')[1]) {
    decimals = price.split('.')[1];
    decimals = '<span class=\'formatted-currency-decimals\'>' + decimals + '</span>';
  }
  if (price.split(' ').length === 2) {
    // price = price.split(' ')[0] + '&thinsp;' + price.split(' ')[1];
    price = price.split(' ')[0] + ',' + price.split(' ')[1] + decimals;
  }
  return price;
}



sendSize();
sendDigitalContent();


function widget_init() {
  $.each($('.pack-button'), function (i, elem) {
    var sku = elem.dataset.digital_content;
    if (!sku) return;
    // elem.innerHTML = '';
    elem.querySelector('.button_p2p').id = 'ps-' + sku;
    elem.classList.add('pack-button--preloader');
    xsolla_widget_simple('button', sku, '#ps-');

    var priceLeft = $('.left').find('[data-digital_content=\'' + sku + '\']');
    if (priceLeft[0]) {
      priceLeft = priceLeft[0]
    } else {
      return;
    };
    priceLeft.classList.add('pack_price--preloader');
    // priceLeft.classList.remove('pack_price--preloader');
    if (isMobile) {
      elem.classList.remove('pack-button--preloader');
      return;
    };
    $(elem).bind("DOMSubtreeModified", function () {
      // console.log('changed');
      setTimeout(function () { //TODO: переделать в Callback
        elem.classList.remove('pack-button--preloader');
        var price = elem.querySelector('.formatted-currency ').textContent;
        price = mikePrice(price);
        priceLeft.innerHTML = price;
        priceLeft.classList.remove('pack_price--preloader');
        elem.querySelector('.formatted-currency').innerHTML = price;
        if (elem.classList.contains('t-shirt')) {
          // $(elem.querySelector('.xpay2play-widget-simple')).css('pointer-events', 'none');
        }
      }, 600);
    });
  });





    $('.buy-t-shirt').click(function () {
      var sku = this.dataset.sku;
      var size = this.dataset.size;
      console.log('size = ', size);
      xsolla_widget_simple('launch', sku, false, size, true)
    });



}

function sendDigitalContent() {
    $('.hero-button.t-shirt').click(function () {
        // access_data_tshirt.purchase.pin_codes.codes[0].digital_content = $(this).data('digital_content');
      var sku = $(this).data('digital_content');
      $('#ps-tshirt')[0].dataset.sku = sku;
    });
}

function sendSize() {
    $('.tshirt-w .text-block-4').click(function () {
        $('.tshirt-w .text-block-4').removeClass('selected');
        $(this).addClass('selected');
        var size = $(this).text().replace(/\s/g, '');
        // access_data_tshirt.custom_parameters.size = size;
        $('#ps-tshirt')[0].dataset.size = size;
    });
}




var s = document.createElement('script');
s.type = "text/javascript";
s.async = true;
// s.src = "https://static.xsolla.com/embed/paystation/1.0.7/widget.min.js";
// s.src = "https://static.xsolla.com/embed/pay2play/2.1.3/widget.js";
// s.src = "http://xsolla.maiik.ru/phoenix_point/pay2play/2.1.3/widget.js";
s.src = "//livedemo.xsolla.com/phoenixpoint/phoenix-point(31-oct-2017)-1/js/widget.js";
s.addEventListener('load', function (e) {
  widget_init();
}, false);
var head = document.getElementsByTagName('head')[0];
head.appendChild(s);


