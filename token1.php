<?php
header('Content-type: application/json');
$merchant_id = 38951;//Phoen
// $merchant_id = 2340; //Test Shop

$api_key = '5QB0crW0ZmTF5Ihw'; //Phoen
// $api_key = 'ZHgbSDVP6LtAJVWu';//Test Shop
// Super project 14607



$promo = isset($_GET['promo']) ? htmlspecialchars($_GET['promo']) : '';
$digitalContent = isset($_GET['digital_content']) ? htmlspecialchars($_GET['digital_content']) : '';
$project = isset($_GET['project']) ? (int)$_GET['project'] : '';
// $project = 22877;

$requestArray = array (
    "settings" => array(
        "project_id" => $project
    ),
    "purchase" => array(
        "pin_codes" => array(
            "codes" => array(
                array(
                    "digital_content" => $digitalContent
                )
            )
        )
    ),
    "user" => array (
        "id" => array (
            "value" => microtime(false)
        ),
        "name" => array (
            "value" => "user"
        )
    ),
);
if($promo) {
    $requestArray['user']['attributes'] = array(
        "promo" => $promo
    );
}

$request = json_encode($requestArray);

$ch = curl_init('https://api.xsolla.com/merchant/merchants/'.$merchant_id.'/token');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Accept: application/json',
    'Authorization: Basic ' . base64_encode($merchant_id . ':' . $api_key)
));

$server_output = curl_exec($ch);
if (curl_errno($ch)) {
    print curl_error($ch);
} else {
    curl_close($ch);
}
echo $server_output;


$response = $xsollaClient->GetStorefrontVirtualGroups(array(
  'project_id' => PROJECT_ID,
  'language' => LANGUAGE_ISO,
  'currency' => CURRENCY_ISO,
  'user_id' => USER_ID
));

echo $response;