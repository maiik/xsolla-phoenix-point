// var access_data = {
//     "settings":{
//       "project_id": 22863,
//       "ui": {
//           "size": "medium",
//           "theme": "light"
//         }
//     }
// };

var url = new URL(window.location.href);
var promo = url.searchParams.get("promo");




var lightbox = {
  width: '740px',
  height: '625px',
  spinner: 'round',
  spinnerColor: '#fff',
  // contentBackground: "rgba(0,0,0,0.2)"
  contentBackground: '#000',
};


var access_data_default = {
  "settings":{
    "project_id": 22863,
    "shipping_enabled": false,
    "ui": {
        "size": "medium",
        // "theme": "dark"
      }
    }
}

var access_data_widget = access_data || access_data_default;


function xsolla_widget_simple(sku, idBeginning, i) {

  access_data_user = {
    "id": {
      "value": "john_carter",
      "hidden": true
    },
    "name": {
      "value": "John Carter"
    },
    "country": {
      "allow_modify": true
    }
  };
  access_data_purchase = {
    "pin_codes": {
      "codes": [
        { "digital_content": "" }
      ]
    }
  };

  access_data_widget.user = access_data_user;
  access_data_widget.purchase = access_data_purchase;
  access_data_widget.settings.ui = {"size": "medium", /*"theme": "dark"*/ };


  // var local_simple = 'en';
  var access_data_simple = access_data_widget;
  access_data_simple.purchase.pin_codes.codes[0].digital_content = sku;

  var options_simple = {
      access_data: access_data_simple,
      template: 'simple',
      lightbox: lightbox,
  };
  options_simple['target_element'] = idBeginning + sku + '-' + i;
  // XPay2PlayWidget.create(options_simple);

  $.ajax({
      url: 'https://livedemo.xsolla.com/streets-of-rogue/token.php',
      data: {
          'digital_content': '1',
          'promo': promo,
          'project': 22241
      },
      method: 'get',
      dataType: 'json',
      beforeSend: function () {

      },
      success: function (data) {
          token = data['token'];
          options1.access_token = token;
          XPayStationWidget.init(options_simple);
          XPayStationWidget.open();
      }
  });


}







function widget_init() {
  $.each($('.ps-button'), function (i, elem) {
    elem.innerHTML = '';
    var sku = elem.dataset.digital_content;
    if (!sku) return;
    var newButton = document.createElement('div');
    newButton.classList.add('ps-button-button');
    newButton.id = 'ps-' + sku + '-' + i;
    elem.appendChild(newButton);
    // elem.innerHTML = '';
    // elem.classList.add('button--preloader');
    xsolla_widget_simple(sku, '#ps-', i);
  });
}

var s = document.createElement('script');
s.type = "text/javascript";
s.async = true;
s.src = "https://cdn.xsolla.net/mikelibs/pay2play/2.1.3mike/widget.js";
s.addEventListener('load', function (e) {
  widget_init();
}, false);
var head = document.getElementsByTagName('head')[0];
head.appendChild(s);


