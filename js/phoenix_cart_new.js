
var Xsolla;


var ver = '30mar2018-1658';



var debug = debug || false; //вывод всяких сообщений в консоль


var mainSettings_default =  {
  //Shop Settings
  // 'defaultGrids': 'all', //TODO
  'callbacks': {
    'cartAfterChange': typeof showCartNoti !== 'undefined' ? showCartNoti : null, //Callback when something changed in the cart
    'cartAddAnimation': typeof cartAddAnimation !== 'undefined' ? cartAddAnimation : null,
  },
  'shopSettings': {
    'paystation': {
      'access_data': {
        'user': {
          'attributes': {
            // 'promo': false,
          }
        },
        'settings': {
          'project_id': 24042,
          // 'shipping_enabled': true
          'ui': {
            'size': 'medium',
            'theme': 'dark'
          },
        },
      },
    },
    // 'psInit': function () {
    //   s = document.createElement('script');
    //   s.type = 'text/javascript';
    //   s.async = true;
    //   s.src = '//static.xsolla.com/embed/paystation/1.0.7/widget.min.js';
    //   var head = document.getElementsByTagName('head')[0];
    //   head.appendChild(s);
    // },
    'shopData': [], //Data from Xsolla Merchant. Ex.: http://xsolla.maiik.ru/phoenix_point/atari_json.html
  },
  // 'customAttrs': //array, //Custom Pictures array
  //Cart Settings
  'cartSettings': {
    'indicatorShown': 'auto', //auto
    'cartElements': {
      'cartWrapper':        '.k',
      'cartCounter':        '[data-kart=\'counter\']',
      'cartIcon':           '[data-kart=\'icon\']',
      'cartUL':             '[data-kart=\'ul\']',
      'cartItemTemplate':   '[data-kart=\'template\']',
      'cartAddGlow':        '[data-kart=\'glow\']',
      'cartTotal':          '[data-kart=\'total\']',
      'cartDiscountPrice':  '[data-kart=\'total_discount\']',
      'cartOpenClass':      '[data-kart=\'open\']',
      'cartCloseClass':     '[data-kart=\'close\']',
      'cartCheckoutBut':    '[data-kart=\'checkout\']',
      'cartClear':          '[data-kart=\'clear\']',
      // 'cartAddBut':      ':attr(\'^data-kart-add\')',
    },

    'cartAddDiscount': typeof cartAddDiscount !== 'undefined' ? cartAddDiscount : null,
    'cartOpenAnimation': typeof cartOpenAnimation !== 'undefined' ? cartOpenAnimation : null,
    'cartCloseAnimation': typeof cartCloseAnimation !== 'undefined' ? cartCloseAnimation : null,
    'cartShowAnim': false,
    'cartHideAnim': false,
    // 'currency': ['₽', 1],
    'currency': ['$', 0],
  }
};








var phoenix_custom_attributes = {
  '01': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fd830e00001bde30e_Tier1_GIF_1___small.gif',
    // 'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb5fe541771000181c5a5_Tier1_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb604541771000181c5a6_Tier1_GIF_1.gif'
  },
  '02': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aae_Tier2_GIF_1___short.gif',
    // 'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb5fea510b60001abcd3a_Tier2_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6054d8d810001dff53d_Tier2_GIF_1.gif'
  },
  '03': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aaf_Tier3_GIF___short.gif',
    // 'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6cd71bd9600015f8928_Tier3_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6ce541771000181c659_Tier3_GIF_1.gif'
  },
  '05': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aaf_Tier3_GIF___short.gif',
    // 'image_url_custom': 'http://uploads.webflow.com/5a03819163f9620001c8ecbe/5a0be36c01d2510001c671e0_Tier3GIF_2MB25fps_still.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0be2b4ef4a3c00017fc3b4_Tier3GIF_2MB25fps.gif',
    'shipping_enabled': true,
  },
  '06': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aaf_Tier3_GIF___short.gif',
    // 'image_url_custom': 'http://uploads.webflow.com/5a03819163f9620001c8ecbe/5a0be36c01d2510001c671e0_Tier3GIF_2MB25fps_still.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0be2b4ef4a3c00017fc3b4_Tier3GIF_2MB25fps.gif',
    'shipping_enabled': true,
  },

  'ART_BOOK': {
    'shipping_enabled': true,
  },

  'COMPENDIUM_PRINT': {
    'shipping_enabled': true,
  },

  'KEY_RING_ADD': {
    'shipping_enabled': true,
  },

  'POSTER': {
    'shipping_enabled': true,
  },

  'T_SHIRT': {
    'shipping_enabled': true,
  },

}






//el — новая строка
//already — true/false
function cartAddAnimation1(el, already) {

}

function cartAddDiscount1(q) { //TODO: сделать подсчет скидки более универсальным, передавать не кол-во, а состав корзины
  var discount = 0;
  var offvalue = false;
  return [discount, offvalue];
}





var notiShown = false;

$('[data-shop-kart=\'plural\']').each(function (i, item) {
  item.dataset.originalWord = item.textContent
});

function showCartNoti(thiss) {
  var notiEl = $(':attr(\'^data-kart-noti\')');
  var ixNotiHide = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 250ms ease 0ms',
      'x': '0px',
      'y': '80px',
      'z': '0px',
    }],
    'stepsB': []
  };


  if (thiss['q'] > 0) {
    setTimeout(function () {
      ix.run(ixResetXY, notiEl);
    }, 50)
  } else {
    setTimeout(function () {
      ix.run(ixNotiHide, notiEl);
    }, 50)
  }


  //Pluralize
  var $plurals = $('[data-shop-kart=\'plural\']');
  $plurals.each(function (i, item) {
    var word = item.dataset.originalWord || item.textContent;
    if (thiss['q'] > 1) {
      item.textContent = pluralize(word);
    } else {
      item.textContent = word;
    }
  });

}






qq(true);
function qq(hide) {
  // if (!xsolla) xsolla = new Xsolla(shop);
  if (hide) {
    $('.q').removeClass('shown');
    $('.b').removeClass('xsolla_panel');
    $('.float').removeClass('xsolla_panel');
    return
  }
  $('.q').addClass('shown');
  $('.b').addClass('xsolla_panel');
  $('.float').addClass('xsolla_panel');

}




// $(':attr(\'^data-q-close\')').click(function () {
//   qq(true)
// })


$('.k').removeClass('shown');









var notiShown = false;

$('[data-shop-kart=\'plural\']').each(function (i, item) {
  item.dataset.originalWord = item.textContent
});

function showCartNoti(thiss) {
  var notiEl = $(':attr(\'^data-kart-noti\')');
  var ixNotiHide = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 250ms ease 0ms',
      'x': '0px',
      'y': '80px',
      'z': '0px',
    }],
    'stepsB': []
  };


  if (thiss['q'] > 0) {
    setTimeout(function () {
      ix.run(ixResetXY, notiEl);
    }, 50)
  } else {
    setTimeout(function () {
      ix.run(ixNotiHide, notiEl);
    }, 50)
  }


  //Pluralize
  var $plurals = $('[data-shop-kart=\'plural\']');
  $plurals.each(function (i, item) {
    var word = item.dataset.originalWord || item.textContent;
    if (thiss['q'] > 1) {
      item.textContent = pluralize(word);
    } else {
      item.textContent = word;
    }
  });

}






  // qq(true);

  // function qq(hide) {
  //   // if (!xsolla) xsolla = new Xsolla(shop);
  //   if (hide) {
  //     $('.q').removeClass('shown');
  //     $('.b').removeClass('xsolla_panel');
  //     $('.float').removeClass('xsolla_panel');
  //     return
  //   }
  //   $('.q').addClass('shown');
  //   $('.b').addClass('xsolla_panel');
  //   $('.float').addClass('xsolla_panel');

  // }





























  var ix_But_addToCart_1 = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
      'x': '-102px',
    }]
  };

  var ix_But_addToCart_0 = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
      'x': '0px',
    }]
  };


  var ix_But_addBuy_1 = {
    'stepsA': [{
      'opacity': 0,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    }]
  };

  var ix_But_addBuy_0 = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    }]
  };



  var kartItemInitialHeight = 130;

  var ixKItemAppear = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 100ms ease 0ms, opacity 100ms ease 0ms',
      'height': '130px',
    }]
  };



  var ixKPlus = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
      'scaleX': 1,
      'scaleY': 1,
      'scaleZ': 1,
      // 'y': '-26px',
    }]
  };



  var ixKPlus = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
      'scaleX': 1,
      'scaleY': 1,
      'scaleZ': 1,
      // 'y': '-26px',
    }]
  };

  var ixKMinus = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
      'scaleX': 1,
      'scaleY': 1,
      'scaleZ': 1,
      // 'y': '26px',
    }]
  };

  var ixKPlusMinusOff = {
    // 'stepsA': [{
    //   'opacity': 0,
    //   'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    //   'scaleX': 1,
    //   'scaleY': 1,
    //   'scaleZ': 1,
    //   // 'y': '0px',
    // }]
  };



  var ix_qChange_1 = {
    'stepsA': [{
      'opacity': 1,
      'scale': 1,
      'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
    }]
  };
  var ix_qChange_0 = {
    'stepsA': [{
      'opacity': 1,
      'scale': 1,
      'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
    }]
  };












//Эти идут
var shopSettings_PP =
  {
    'callbacks': {
      'callbackAfterStore': addScrollTo,
      'cartAterChange': typeof showCartNoti !== 'undefined' ? showCartNoti : null, //Callback when something changed in the cart
    },
    'shopSettings': {
      'paystation': {
        'access_data': {
          'user': {
            'attributes': {
              // 'promo': false,
            }
          },
          'settings': {
            'project_id': 24042,
            // 'shipping_enabled': true
            'ui': {
              'size': 'medium',
              'theme': 'dark'
            },
          },
        },
      },
      'defaultGrids':
        [
          {
            'groupId': 6660,
            'template': 'template1'
          },
          {
            'groupId': 'all'
          }
        ],
    },
    'cartSettings': {
      'indicatorShown': 'auto', //auto
      'cartShown': 'auto', //auto
      'cartAddAnimation': typeof cartAddAnimation !== 'undefined' ? cartAddAnimation : null,
      'cartAddDiscount': typeof cartAddDiscount !== 'undefined' ? cartAddDiscount : null,
      'currency': ['$', 0],
    },

    'customAttrs': phoenix_custom_attributes, //Custom Pictures and shipping
    'noDynamicStoreftont': false,

    // 'defaultGrids': [
    //   {
    //     'where': '#grid_mainpacks_new',
    //     'typeName': 'group',
    //     'typeValue': 'Mainpacks',
    //     'title': 'Phoenix Point Packs',
    //   },
    //   {
    //     'where': '#grid_addons',
    //     'typeName': 'group',
    //     'typeValue': 'Addons',
    //     'title': 'Phoenix Point Addons',
    //   },
    // ]
};





















var psInit = null;
// var XPayStationWidget = null;
var phoenixShop = null;

requirejs.config({
  baseUrl: 'http://xsolla.maiik.ru/xsolla_store/js/',
  paths: {
    // 'swiper': 'http://xsolla.maiik.ru/libs/swipe',
    'swiper': 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min',
    'xsolla_store': 'http://xsolla.maiik.ru/xsolla_store/js/xsolla_store',
    'fragbuilder': 'http://xsolla.maiik.ru/xsolla_store/libs/fragbuilder',
  }
});

define([
  'xsolla_store',
], function (Xsolla) {

  function phoenixShop() {
    this.xsolla = new Xsolla(shopSettings_PP);
  }
  window.phoenixShop = new phoenixShop();
  addScrollTo();
  return phoenixShop;
});


