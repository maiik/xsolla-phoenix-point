var ix_But_addToCart_1 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '-102px',
  }]
};

var ix_But_addToCart_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '0px',
  }]
};


var ix_But_addBuy_1 = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};

var ix_But_addBuy_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};


var phoenix_custom_pics = {
  '01': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb5fe541771000181c5a5_Tier1_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb604541771000181c5a6_Tier1_GIF_1.gif'
  },
  '02': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb5fea510b60001abcd3a_Tier2_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6054d8d810001dff53d_Tier2_GIF_1.gif'
  },
  '03': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6cd71bd9600015f8928_Tier3_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6ce541771000181c659_Tier3_GIF_1.gif'
  },
  '05': {
    'image_url_custom': 'http://uploads.webflow.com/5a03819163f9620001c8ecbe/5a0be36c01d2510001c671e0_Tier3GIF_2MB25fps_still.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0be2b4ef4a3c00017fc3b4_Tier3GIF_2MB25fps.gif'
  },
  '06': {
    'image_url_custom': 'http://uploads.webflow.com/5a03819163f9620001c8ecbe/5a0be36c01d2510001c671e0_Tier3GIF_2MB25fps_still.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0be2b4ef4a3c00017fc3b4_Tier3GIF_2MB25fps.gif'
  },
}



function cartAddAnimation1(el, already) {
  var butCart = el.parentElement.parentElement.querySelector('.ix_bt_cart');
  var butBuy = el.parentElement.parentElement.querySelector('.ix_bt_buy');
  var carticon = el.parentElement.parentElement.querySelector('.ix_bt_carticon');
  var crtdelicon = el.parentElement.parentElement.querySelector('.k_delete');

  if (el.classList.contains('k_li_upsale_item')) return;
  if (already) {
    // this.element.parentElement.parentElement.classList.add('already');
    ix.run(ix_But_addToCart_1, butCart);
    ix.run(ix_But_addBuy_1, butBuy);
    carticon.classList.add('hidden');
    crtdelicon.classList.remove('hidden');
    // setTimeout(function() {
      butBuy.classList.add('invisible');
    // }, 100);
  } else {
    // this.element.parentElement.parentElement.classList.remove('already');
    ix.run(ix_But_addToCart_0, butCart);
    ix.run(ix_But_addBuy_0, butBuy);
    crtdelicon.classList.add('hidden');
    carticon.classList.remove('hidden');
    // setTimeout(function() {
      butBuy.classList.remove('invisible');
    // }, 200);
  }
}

function cartAddDiscountAAAATARI(q) { //TODO: сделать подсчет скидки более универсальным, передавать не кол-во, а состав корзины
  var discount = 0;
  var offvalue = false;
  var discountBlock = document.querySelector('#kart_discount');
  $(discountBlock).find('.active').removeClass('active').addClass('inactive');
  // if (this.cartCont.querySelector('#kart_discount')) {
  if (q < 2) {
    // this.cartCont.removeChild(this.cartCont.querySelector('#kart_discount'));
    discount = 0;
    offvalue = false;
  }
  if (q >= 2 && q < 4 ) {
    discount = 15;
    $(discountBlock).find('[data-cart-discount="' + discount + '"]').addClass('active').removeClass('inactive');
    offvalue = '15OFF';
  }
  if (q >= 4) {
    discount = 25;
    // $(this.discountBlock).find('.active').addClass('active');
    $(discountBlock).find('[data-cart-discount="' + discount + '"]').addClass('active').removeClass('inactive');
    offvalue = '25OFF';
  }

  return [discount, offvalue];
}


var notiShown = false;
function showCartNoti(thiss) {
  var notiEl = $(':attr(\'^data-kart-noti\')');
  var ixNotiHide = {
    'stepsA': [{
      // 'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 250ms ease 0ms',
      'x': '0px',
      'y': '80px',
      'z': '0px'
    }],
    'stepsB': []
  };

  if (thiss['q'] > 0) {
    ix.run(ixResetXY, notiEl);
  } else {
    ix.run(ixNotiHide, notiEl);
  }
}


var shop;

var shopSettings =
  {
    //Shop Settings
    'shopData': [], //Data from Xsolla Merchant. Ex.: http://xsolla.maiik.ru/phoenix_point/atari_json.html
    'cartAddBut': ':attr(\'^data-cart-add\')',

    'shopDraw': {
      'where': '',
      'typeName': ''
    },
    'customPics': phoenix_custom_pics, //Custom Pictures

    //Cart Settings
    'cartSettings': {
      'indicatorShown': 'auto', //auto
      'afterChange': typeof showCartNoti !== 'undefined' ? showCartNoti : null, //Callback when something changed in the cart
      'cartAddAnimation': typeof cartAddAnimation !== 'undefined' ? cartAddAnimation : null,
      'cartAddDiscount': typeof cartAddDiscount !== 'undefined' ? cartAddDiscount : null,
      'paystation': {
        'access_data': {
          'user': {
            'attributes': {
              // 'promo': false,
            }
          },
          'settings': {
            'project_id': '',
            // 'shipping_enabled': true
          },
          'purchase': {
            'virtual_items': {
              'items': []
            }
          }
        },
        'lightbox': {
          'width': '740px',
          'height': '685px',
          'spinner': 'round',
          'spinnerColor': '#cccccc',
        }
      },
      'cartShowAnim': ' ',
      'cartHideAnim': ' ',
      'cartWrapper': '.k',
      'cartCounter': ':attr(\'^data-kart-counter\')',
      'cartItemTemplate': ':attr(\'^data-kart-template\')',
      'cartIcon': '#top_cart',
      'cartContainer': '#kart',
      'cartTotal': ':attr(\'^data-kart-total\')',
      'cartOpenClass': '.k_ix_show',
      'cartCloseClass': '.k_ix_hide',
      'cartCheckoutBut': '#button_buy--checkout',
      'cartDiscountPrice': '#kart_tot_discount',
      'cartClear': ':attr(\'^data-kart-clear\')',
      // 'currency': ['₽', 1],
      'currency': ['$', 0],
      'psInit': function () {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = '//static.xsolla.com/embed/paystation/1.0.7/widget.min.js';
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(s);
      }
    }
  };

var ixKPlus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    // 'y': '-26px',
  }]
};



var ixKPlus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    // 'y': '-26px',
  }]
};

var ixKMinus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    // 'y': '26px',
  }]
};

var ixKPlusMinusOff = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    // 'y': '0px',
  }]
};



var ix_qChange_1 = {
  'stepsA': [{
    'opacity': 1,
    'scale': 1,
    'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
  }]
};
var ix_qChange_0 = {
  'stepsA': [{
    'opacity': 1,
    'scale': 1,
    'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
  }]
};


var Xsolla;

function init(access_data) {
  $.ajax({
      url: 'https://secure.xsolla.com/paystation2/api/virtualitems/',
      data: {
          access_data: JSON.stringify(access_data)
      },
      method: 'post',
      dataType: 'json',
      beforeSend: function () {

      },
      success: function (items_data) {
          console.log(items_data);
          //document.querySelector('#jsn').innerText = JSON.stringify(data['groups'][0]['virtual_items']);
          //document.querySelector('#jsn').innerHTML = syntaxHighlight(data['groups'][0]['virtual_items']);
          //document.querySelector('.prettyprint').innerHTML = JSON.stringify(data['groups'][0]['virtual_items']);

        shopSettings['shopData'] = items_data['groups'];
        // shopSettings['cartSettings'].paystation.access_data.project_id = access_data.project_id;
        shopSettings['cartSettings'].paystation.access_data.settings.project_id = access_data.settings.project_id;
        shopSettings['defaultGrids'] = [
          {
            'where': '#grid_mainpacks_new',
            'typeName': 'group',
            'typeValue': 'Mainpacks',
            'title': 'Phoenix Point Packs',
          },
          {
            'where': '#grid_addons',
            'typeName': 'group',
            'typeValue': 'Addons',
            'title': 'Phoenix Point Addons',
          },
        ];


        // shop = new Shop(shopSettings);
        // xsolla = new Xsolla(shopSettings);


        //TEMP OLD STYLE
        // dataObj = shop._data;
        // drawItemsType('#grid_mainpacks_new', 'group', 'Mainpacks');
        // // drawItemsType('#grid_mainpacks', 'group', 'Mainpacks');
        // drawItemsType('#grid_addons', 'group', 'Addons');
        // shop.collectButtons();


        qq();

        addScrollTo(); //CLICK on buy scrolls


        // hideElements(itemsContainer);
      }
  });
}


xsolla = new Xsolla(shopSettings, addScrollTo); //Callback adds scroll


// var access_data_phoenix = {
//   "settings": {
//     "project_id": 24042
//   }
// };

// init(access_data_atari);
// init(access_data_phoenix);



//22877




////////////////////   PANEL   //////////////////////

var xsolla;
function qq(hide) {
  if (!xsolla) xsolla = new Xsolla(shop);
  if (hide) {
    $('.q').removeClass('shown');
    return
  }
  $('.q').addClass('shown');
}




$(':attr(\'^data-q-close\')').click(function () {
  qq(true)
})
