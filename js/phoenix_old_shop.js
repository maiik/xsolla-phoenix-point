

var itemsContainer = document.querySelector('.grid');


var popC = document.querySelector('#pop_content');

var ix = Webflow.require('ix');
var ixShowOpacity = {'stepsA': [{'opacity': 0}, {'opacity': 1, 'transition': 'opacity 200ms ease 0ms'}], 'stepsB': []};
var ixHideOpacity = { 'stepsA': [{ 'opacity': 1 }, { 'opacity': 0, 'transition': 'opacity 200ms ease 0ms' }], 'stepsB': [] };
var ixHideOpacityLong = {'stepsA': [{'opacity': 1}, {'opacity': 0, 'transition': 'opacity 800ms ease 0ms'}], 'stepsB': []};
var ixHideOpacityFast = { 'stepsA': [{ 'opacity': 1 }, { 'opacity': 0, 'transition': 'opacity 20ms ease 0ms' }], 'stepsB': [] };

var dataArray = [];
var dataCurrent = dataArray;


var ixItmHide = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 0ms ease 0ms, opacity 0ms ease 0ms',
    'x': '0px',
    'y': '10px',
    'z': '0px',
  }]
}
var ixItmShow = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '0px',
    'y': '0px',
    'z': '0px',
  }]
}

var ixItemApp = {
  'stepsA': [{
    'opacity': 1, 'transition': 'transform 250ms ease 0ms, opacity 250ms ease 0ms', 'x': '0px', 'y': '0px', 'z': '0px'
  }],
  'stepsB': []
};

var ixItemDiss = {
  'stepsA': [{
      'opacity': 0, 'transition': 'transform 100ms ease 0ms, opacity 100ms ease 0ms', 'x': '0px', 'y': '20px', 'z': '0px'
    }],
  'stepsB': []
};

//вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ” POP OPEN вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”

var ixPicSize = {
  'stepsA': [{
    // 'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scale': (3, 3),
    'y': 40 + 'px',
  }]
}

var ixTxtShow = {
  'stepsA': [{
    // 'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    // 'x': newX + 'px',
    'y': 250 + 'px',
    // 'scale': (2,2)
  }]
}


var ixButSize = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms',
    'scaleX': 1.4,
    'scaleY': 1.4,
    'y': -10 + 'px',
  }]
}


var ixMoveIcUp = {
  'stepsA': [{
    'transition': 'transform 10ms',
    'x': '0px',
    'y': '-500px',
    'z': '0px'
  }], 'stepsB': [{}]
};




//вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”HOVERвЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”


var ixHvrOnBg = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'x': '0px',
    'y': '0px',
    'z': '0px',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1
  }], 'stepsB': [{}]
};

var ixHvrOffBg = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'scaleX': 0.4, 'scaleY': 0.4, 'scaleZ': 1, 'x': '0px', 'y': '-100px', 'z': '0px'
  }], 'stepsB': [{}]
};

var ixHvrOnPic = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'x': '0px', 'y': '4px', 'z': '0px',
    'scaleX': 1.1,'scaleY': 1.1,
  }], 'stepsB': [{}]
};


var ixHvrOffPic = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'x': '0px', 'y': '0px', 'z': '0px',
    'scaleX': 1, 'scaleY': 1,
  }], 'stepsB': [{}]
};

var ixHvrOnTxt = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'x': '0px', 'y': '8px', 'z': '0px'
  }], 'stepsB': [{}]
};

var ixHvrOffTxt = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'x': '0px', 'y': '0px', 'z': '0px'
  }], 'stepsB': [{}]
};

var ixHvrOnBgf = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'scale': (1.10,1.10)
  }], 'stepsB': [{}]
};

var ixHvrOffBgf = {
  'stepsA': [{
    'opacity': 0.5,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'scale': (1,1)
  }], 'stepsB': [{}]
};



var ixHvrOnButs = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'x': '0px', 'y': '10px', 'z': '0px'
  }], 'stepsB': [{}]
};

var ixHvrOffButs = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '0px', 'y': '0px', 'z': '0px'
  }]
}


var ixHvrOnButsF = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200 ease 0, opacity 200 ease 0',
    'x': '0px', 'y': '10px', 'z': '0px'
  }], 'stepsB': [{}]
};

var ixHvrOffButsF = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '0px', 'y': '0px', 'z': '0px'
  }]
}


// вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”DRAW ITEMSвЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”



function ixShow(i, element) {
  switch (i) {
    case 1:
      ixMoveIcDown = { 'stepsA': [{'transition': 'transform 200ms ease-out 0ms', 'x': '0px', 'y': '0px', 'z': '0px' }], 'stepsB': [{}] };
      break;
    case 2:
      ixMoveIcDown = { 'stepsA': [{'transition': 'transform 200ms ease-in-cubic 0ms', 'x': '0px', 'y': '600px', 'z': '0px' }], 'stepsB': [{}] };
      break;
  }
  ix.run(ixMoveIcDown, element);
}
function hideElements(el) {
  items1 = el.querySelectorAll('.pop_item');
  var i = items1.length;
  function f() {
    var element = items1[i];
    ixShow(2, element);
    i--;
    if (i >= 0) {
      setTimeout(f, 20 + (i / 20) * 20);
    }
  }
  f();
}

function drawItemsType(where, typeName, type) {
  console.log('where = ', where);

  // type = type | 'all';
  //delete default items

  // var template = $(where).find('*[data-template=\'true\']')[0];
  var template = template || $(where).find(':attr(\'^data-good\')')[0];

  // var template = $(where)[0].childNodes[0];

  template.classList.add('hidden');
  var templateClassName = '.' + template.classList[0];

  var itemCont = document.querySelector(where);

  // var items1 = itemCont.querySelectorAll('.grid-item');
  var items1 = itemCont.querySelectorAll(templateClassName);
  for (var i = 0; i < items1.length; i++) {
    var element = items1[i];
    if (element !== template) {
      itemCont.removeChild(element);
    } else {
      element.classList.add('template');
    }
  }

  //cycle through Data Object
  var dataA = Object.keys(dataObj);




  //show default set
  for (var u = 0; u < dataA.length; u++) {
    var obj = dataObj[dataA[u]];

    //todo: TEsMP
    // if (obj.mat != 'scale' || obj.mat != 'plate' || obj.mat != 'padded' || obj.mat != 'leather' || obj.mat != 'chain') {
    //   // return
    // } else {

    var itm;
    if (!typeName) {
      itm = drawItem(obj, template);
      itemCont.appendChild(itm);
      ix.run(ixItmHide, itm);
    } else {
      // if (obj[typeName] === type) {
      if (obj[typeName].indexOf(type) >= 0) {
        itm = drawItem(obj, template);
        itemCont.appendChild(itm);
        ix.run(ixItmHide, itm);
      }
    }
    // }
  }
  addHover(where);
  showElements(where)
}



function showPop(id) {
  document.querySelector('.p').classList.add('shown');
  popC.parentNode.appendChild(drawItem(dataObj[id], popC, true));
}

function hidePop() {
  document.querySelector('.p').classList.remove('shown');
}
$('.pzz').click(function () {
  hidePop()
})


function drawItem(obj, template, noClone) {


  var item;
  if (noClone) {
    item = template;
  } else {
    item = template.cloneNode(true);
    item.id = '';
  };

  item.dataset.idd = obj.sku;
  item.classList.remove('hidden', 'template');
  item.dataset.good = null;
  item.dataset.template = null;
  // item.querySelector('.c_pic').style.backgroundImage = 'url(' + obj.pic + ')';
  // item.querySelector('.c_bg_blur').style.backgroundImage = 'url(' + obj.pic + ')';
  // item.querySelector('.c_txt1_name').innerHTML = obj.name;
  // item.querySelector('.c_txt1_price').innerHTML = '$' + obj.price;
  // item.querySelector('.c_txt1_desc').innerHTML = obj.desc1;

  $(item).find('[data-good=\'pic\']')[0].style.backgroundImage = 'url(' + obj.image_url + ')';

  if ($(item).find('[data-good=\'pic_bg\']')[0]) {
    $(item).find('[data-good=\'pic_bg\']')[0].style.backgroundImage = 'url(' + obj.image_url + ')';
  }

  $(item).find('[data-good=\'name\']')[0].innerHTML = obj.name;
  $(item).find('[data-good=\'desc\']')[0].innerHTML = obj.description;
  $(item).find('[data-good=\'price\']')[0].innerHTML = '$' + obj.amount;


  var butsArr = $(item).find('[data-cart-add=\'true\']');
  Array.prototype.forEach.call(butsArr, function (element) {
    element.dataset.cartAdd = obj.sku;
  }, this);


  if (obj.featured) {
    item.classList.add('featured');
  }
  if (obj.expire) {
    // $(item).find('.expire').classList.add('')
  } else {
    if ($(item).find('[data-good=\'expire\']')[0]) {
      $(item).find('[data-good=\'expire\']')[0].classList.add('hidden');
    }
  }
  if (obj.priceNew) {
    $(item).find('[data-good=\'price_old\']')[0].innerHTML = '$' + obj.amount_without_discount;
    // item.querySelector('.c_txt1_price_old').innerHTML = '$' + obj.priceNew;
  } else {
    // var ell = item.querySelector('.c_txt1_price_old');
    // var ell = $(item).find('[data-good=\'price_new\']')[0];
    // ell.parentNode.removeChild(ell);
  }

  // if (obj.fav) {
  //   if ($(item).find('[data-badge=\'fav\']')) {
  //     $(item).find('[data-badge=\'fav\']')[0].classList.add('active');
  //   }
  // }



  if ($(item).find('.item_buts')[0]) {
    if (obj.preorder) {
      item.querySelector('.buts_main_buy').classList.add('hidden');
    } else {
      item.querySelector('.buts_main_preorder').classList.add('hidden');
    }
  }


  // item.querySelector('.pop_item_more').textContent = obj.desc2;

  // if (obj['pType'] === 'bucks') {
  //   item.querySelector('.coin').classList.add('hidden');
  //   item.querySelector('.buck').classList.remove('hidden');
  // }

  // item.dataset.type = obj['pType'];
  // item.dataset.price = obj['price'];
  return item;
}




function ixShow(onoff, element) {
  switch (onoff) {
    case 1:
      ix.run(ixItemApp, element);
      break;
    case 2:
      ix.run(ixItemDiss, element);
      setTimeout(function() {
        element.parentNode.removeChild(element);
      }, 201);
      break;
  }
}

function hideElements(where) {
  var items = where.querySelectorAll('.grid-item');
  var i = items.length;
  function f() {
    var element = items[i];
    ixShow(2, element);
    i--;
    if (i >= 0) {
      setTimeout(f, 50);
    }
  }
  f();
}

function showElements(where) {
  var items = document.querySelector(where).querySelectorAll('.grid-item');
  // upAll();
  var i = 0;
  function f() {
    var element = items[i];
    ixShow(1, element);
    i++;
    if (i <= items.length - 1) {
      // setTimeout(f, 10 + ((items.length - i) / 5) * 10);
      setTimeout(f, 50);
    }
    // if (i === items.length - 1) {
    //   masonry();
    // }
  }
  f();
}




function upAll() {
  items1 = itemsContainer.querySelectorAll('.pop_item');
  for (var i = 0; i < items1.length; i++) {
    var element = items1[i];
    ix.run(ixMoveIcUp, element);
  }
}




// вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”HOVERвЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”


function elHoverOn(el) {
  el.style.zIndex = 9;
  var bg = el.querySelector('.c_bg_hover');
  var bgF = el.querySelector('.c_bg_feat');
  var pic = el.querySelector('.c_pic');
  var txt = el.querySelector('.c_txt');
  var buts = el.querySelector('.ix_actbuts');
  var butsF = el.querySelector('.buts_main_feat');
  // var but = el.querySelector('.c_bg_hover');
  ix.run(ixHvrOnBg, bg);
  ix.run(ixHvrOnPic, pic);
  ix.run(ixHvrOnTxt, txt);
  ix.run(ixHvrOnBgf, bgF);
  // ix.run(ixHvrOnBg, text);
  // ix.run(ixHvrOnBg, but);
  if (buts) {
    setTimeout(function () {
      ix.run(ixHvrOnButs, buts);
    }, 100)
  }
  if (butsF) {
    ix.run(ixHvrOnButsF, butsF);
  }
}


function elHoverOut(el) {
  el.style.zIndex = 0;
  var bg = el.querySelector('.c_bg_hover');
  var bgF = el.querySelector('.c_bg_feat');
  var pic = el.querySelector('.c_pic');
  var txt = el.querySelector('.c_txt');
  var buts = el.querySelector('.ix_actbuts');
  var butsF = el.querySelector('.buts_main_feat');
  ix.run(ixHvrOffBg, bg);
  ix.run(ixHvrOffPic, pic);
  ix.run(ixHvrOffTxt, txt);
  ix.run(ixHvrOffBgf, bgF);
  if (buts) {
    setTimeout(function () {
      ix.run(ixHvrOffButs, buts);
    }, 120)
  }
  if (butsF) {
    ix.run(ixHvrOffButsF, butsF);
  }
}


function addHover(where) {
  $(where).find('.grid-item').on({
    mouseenter: function () {
        elHoverOn(this)
    },
    mouseleave: function () {
        elHoverOut(this)
    }
  });

  // вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”CLICKSвЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”


  $('.grid-item1').click(function (evt) {
    if (this.classList.contains('but')) return;

    var thisId = this.dataset.idd;
    showPop(thisId);


  //   // var el = evt.target;
  //   var el = this;
  //   console.log(el);
  //   var dup = el.cloneNode(true);
  //   dup.classList.add('pop');
  //   el.parentElement.parentElement.parentElement.appendChild(dup);

  //   dup.style.zIndex = '100';
  //   var popX = this.getBoundingClientRect().left;
  //   console.log('popX = ', popX);
  //   var popY = this.getBoundingClientRect().top;
  //   console.log('popY = ', popY);
  //   dup.style.position = 'fixed';

  //   dup.style.left = popX + 'px';
  //   dup.style.top = popY + 'px';

  //   // dup.style.top = '0px';
  //   // dup.style.left = '0px';
  //   $('.z')[0].classList.remove('hidden');
  //   ix.run(ixShowOpacity, $('.z')[0]);

  //   var newX = window.innerWidth / 2 - popX - 245;
  //   var newY = window.innerHeight / 2 - popY - 300;
  //   console.log('newX = ', newX);
  //   console.log('newX = ', newY);

  //   var ixPop = {
  //     'stepsA': [{
  //       // 'opacity': 0,
  //       'transition': 'transform 00ms ease 0ms, opacity 200ms ease 0ms',
  //       // 'width': '100%',
  //       // 'height': '100%',
  //       // 'x': '0px',
  //       // 'y': '0px',
  //       'x': newX + 'px',
  //       'y': newY + 'px',
  //       // 'scale': (2,2)
  //     }]
  //   }

  //   var ixPopSize = {
  //     'stepsA': [{
  //       'transition': 'transform 20ms ease 0ms, opacity 200ms ease 0ms',
  //       'width': '480px',
  //       'height': '640px',
  //       // 'x': newX + 'px',
  //       // 'y': newY + 'px',

  //       // 'scale': (2,2)
  //     }]
  //   }

  //   var itemDesc = dup.querySelector('.c_txt');
  //   var itemPic = dup.querySelector('.c_pic');

  //   ix.run(ixPop, dup);
  //   ix.run(ixPicSize, itemPic);
  //   ix.run(ixPopSize, dup.querySelector('.item'));
  //   ix.run(ixTxtShow, itemDesc);
  //   el.classList.add('item_active');

  //   setTimeout(function () {
  //     // dup.querySelector('.c_bg_hover').classList.add('shown');
  //   }, 300);
  //   ix.run(ixButSize, dup.querySelector('.item_buts'));

    //
  });

}






(function () {

  // //FEAT
  // drawItemsType('#grid_0', 'feat', true);

  // //NORMAL
  // // drawItemsType('#grid_0', 'mat', 'Padded');
  // drawItemsType('#grid');

  // // hideElements(itemsContainer);


})();









$('.z').click(function (evt) {
  ix.run(ixHideOpacity, $('.z')[0]);
  setTimeout(function () {
    evt.target.classList.add('hidden');
  }, 500);
  ix.run(ixHideOpacity, $('#active')[0]);
  $('.pop').remove();
  $('.item_active').removeClass('item_active');
  // $('#active')[0].parentElement.removeChild($('#active')[0]);
  // $('#active')[0].classList.add('hidden');
});







// вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”CURRENT GAMEвЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”




var currentGame = 'fake';
function bgChange(newGame, right) {
  var u = right ? 1 : -1;
  var bg = document.querySelector('.bg');

  // searchHide();
  changeData(newGame);
  // gameCats();
  // catHighlight('all', filts);

  setTimeout(function() {
    bg.style.backgroundImage = 'linear-gradient(180deg, transparent, ' + games[newGame]['bgColor'] + '),' +
      'url(' + games[newGame]['bgImg'] + ')';
    document.querySelector('.sites').style.backgroundImage = 'linear-gradient(180deg, ' + hexToRGB(games[newGame]['bgColor'], 0.6) + ', ' + hexToRGB(games[newGame]['bgColor'], 0.6) + '),' +
      'url(' + games[newGame]['bgImg'] + ')';
    document.body.style.backgroundColor = games[newGame]['bgColor'];
    document.body.className = '';
    document.body.classList.add(games[newGame]['class']);

    goodHide();
    myHide();
    gameChange(newGame);
  }, 200);

  var ixBgLeft = {
    'stepsA': [
      {
        'opacity': 0,
        'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
        'x': (1000 * u) + 'px', 'y': '0px', 'z': '0px'
      }, {
        'x': (1000 * u * -1) + 'px', 'y': '0px', 'z': '0px'
      }, {
        'opacity': 0.4, 'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms', 'x': '0px', 'y': '0px', 'z': '0px'
      }
    ],
    'stepsB': []
  }
  ix.run(ixBgLeft, bg);
}





function changeData(newGame) { //TODO: todelete
  asset = window['data_' + newGame];
  // pageGoods = Object.keys(asset).slice(goodsToShowFrom, goodsToShowTo);

}



// вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”MASONRY, SLIDERвЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”




// $('.grid').packery(
//   {
//     itemSelector: '.grid-item',
//     // gutter: 20,
//     columnWidth: 240,
//     rowHeight: 300,
//   }
// );


var swipes = [];


var cartPayments = new Swiper('#cart_payments', {
    // Optional parameters
    slidesPerSlide : 1,
    direction: 'horizontal',
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    noSwipingClass: 'swiper-no-swiping',
    slideActiveClass: 'swiper-slide-active',
    centeredSlides: true,
    width: 580,
    height: 400,
    // spaceBetween: 30,
    setWrapperSize: true,
    //parallax: true,
    // slidesOffsetBefore: 8,
    // slidesOffsetAfter: 0,
    roundLengths: true,
    grabCursor: true,
    autoHeight: true,
});


var topSlider = new Swiper('#top_slider', {
  // Optional parameters
  slidesPerSlide : 1,
  direction: 'horizontal',
  pagination: '.swiper-pagination',
  nextButton: '.swiper-button-next',
  prevButton: '.swiper-button-prev',
  noSwipingClass: 'swiper-no-swiping',
  slideActiveClass: 'swiper-slide-active',
  centeredSlides: true,
  // spaceBetween: 30,
  setWrapperSize: true,
  //parallax: true,
  // slidesOffsetBefore: 8,
  // slidesOffsetAfter: 0,
  roundLengths: true,
  grabCursor: true,
  autoHeight: true,
});












$('[data-cart-pay=\'card\']').click(function (evt) {
  // mySwiper.slideTo(1, speed, runCallbacks);
  cartPayments.slideTo(0);
})

$('[data-cart-pay=\'cash\']').click(function (evt) {
  // mySwiper.slideTo(1, speed, runCallbacks);
  cartPayments.slideTo(1);
})
$('[data-cart-pay=\'paypal\']').click(function (evt) {
  // mySwiper.slideTo(1, speed, runCallbacks);
  cartPayments.slideTo(2);
})
$('[data-cart-pay=\'skrill\']').click(function (evt) {
  // mySwiper.slideTo(1, speed, runCallbacks);
  cartPayments.slideTo(3);
})
$('[data-cart-pay=\'700\']').click(function (evt) {
  // mySwiper.slideTo(1, speed, runCallbacks);
  cartPayments.slideTo(4);
})



$('.k_pay_select_li').click(function (evt) {
  // console.log('evt = ', this);
  if (this.classList.contains('active')) return;
  $('.k_pay_select_li').removeClass('active');
  this.classList.add('active');
});


// вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”MASONRY, SLIDERвЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”вЂ”



// $(document).ready(function(){
//   $("#sticker").sticky({topSpacing:0});
// });





// $('[data-filter-cat]').click(function (evt) {
//   // evt.stopPropagation();
//   var cat = evt.target;
//   console.log('cat = ', cat);

// });

jQuery.expr.pseudos.attr = $.expr.createPseudo(function(arg) {
  var regexp = new RegExp(arg);
  return function(elem) {
      for(var i = 0; i < elem.attributes.length; i++) {
          var attr = elem.attributes[i];
          if(regexp.test(attr.name)) {
              return true;
          }
      }
      return false;
  };
});



$(':attr(\'^data-filter-\')').on({
  click: function (evt) {

    //find li
    evt.stopPropagation();
    var el = evt.target;
    if (!el.classList.contains('f_li') ) {
      el = el.parentElement;
    }


    //read key
    var filt = $.each(el.attributes, function(i, att){
      if(att.name.indexOf('data-validate')==0){
          console.log(att.name);
      }
    })[1];

    //get cat
    var cat = filt.name.split('-')[2];
    var catName = el.getAttribute(filt.name);
    catName = catName.split('-')[1];


    if (cat === 'all') {
      drawItemsType('#grid');
      //TODO: mark all Alls
      return;
    }
    if (catName === 'All') {
      drawItemsType('#grid');
    } else {
      //show items
      drawItemsType('#grid', cat, catName, true);
    }

    //mark active
    $(el).parent().children().removeClass('active');
    el.classList.add('active');

  }
});