$('[data-shop-kart=\'plural\']').attr('data-original-word', $('[data-shop-kart=\'plural\']').text());



var phoenix_custom_attributes = {
  '01': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fd830e00001bde30e_Tier1_GIF_1___small.gif',
    // 'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb5fe541771000181c5a5_Tier1_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb604541771000181c5a6_Tier1_GIF_1.gif'
  },
  '02': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aae_Tier2_GIF_1___short.gif',
    // 'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb5fea510b60001abcd3a_Tier2_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6054d8d810001dff53d_Tier2_GIF_1.gif'
  },
  '03': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aaf_Tier3_GIF___short.gif',
    // 'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6cd71bd9600015f8928_Tier3_GIF_0.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0eb6ce541771000181c659_Tier3_GIF_1.gif'
  },
  '05': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aaf_Tier3_GIF___short.gif',
    // 'image_url_custom': 'http://uploads.webflow.com/5a03819163f9620001c8ecbe/5a0be36c01d2510001c671e0_Tier3GIF_2MB25fps_still.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0be2b4ef4a3c00017fc3b4_Tier3GIF_2MB25fps.gif',
    'shipping_enabled': true,
  },
  '06': {
    'image_url_custom': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a25a96fa945fe00015c7aaf_Tier3_GIF___short.gif',
    // 'image_url_custom': 'http://uploads.webflow.com/5a03819163f9620001c8ecbe/5a0be36c01d2510001c671e0_Tier3GIF_2MB25fps_still.gif',
    'image_url_custom_hover': 'https://daks2k3a4ib2z.cloudfront.net/5a03819163f9620001c8ecbe/5a0be2b4ef4a3c00017fc3b4_Tier3GIF_2MB25fps.gif',
    'shipping_enabled': true,
  },

  'ART_BOOK': {
    'shipping_enabled': true,
  },

  'COMPENDIUM_PRINT': {
    'shipping_enabled': true,
  },

  'KEY_RING_ADD': {
    'shipping_enabled': true,
  },

  'POSTER': {
    'shipping_enabled': true,
  },

  'T_SHIRT': {
    'shipping_enabled': true,
  },

}





var ix_But_addToCart_1 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '-102px',
  }]
};

var ix_But_addToCart_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '0px',
  }]
};


var ix_But_addBuy_1 = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};

var ix_But_addBuy_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};



var kartItemInitialHeight = 130;

var ixKItemAppear = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 100ms ease 0ms, opacity 100ms ease 0ms',
    'height': '130px',
  }]
};





function cartAddAnimation1(el, already) {
  var butCart = el.parentElement.parentElement.querySelector('.ix_bt_cart');
  var butBuy = el.parentElement.parentElement.querySelector('.ix_bt_buy');
  var carticon = el.parentElement.parentElement.querySelector('.ix_bt_carticon');
  var crtdelicon = el.parentElement.parentElement.querySelector('.k_delete');

  if (el.classList.contains('k_li_upsale_item')) return;
  if (already) {
    // this.element.parentElement.parentElement.classList.add('already');
    ix.run(ix_But_addToCart_1, butCart);
    ix.run(ix_But_addBuy_1, butBuy);
    carticon.classList.add('hidden');
    crtdelicon.classList.remove('hidden');
    // setTimeout(function() {
      butBuy.classList.add('invisible');
    // }, 100);
  } else {
    // this.element.parentElement.parentElement.classList.remove('already');
    ix.run(ix_But_addToCart_0, butCart);
    ix.run(ix_But_addBuy_0, butBuy);
    crtdelicon.classList.add('hidden');
    carticon.classList.remove('hidden');
    // setTimeout(function() {
      butBuy.classList.remove('invisible');
    // }, 200);
  }
}

function cartAddDiscountAAAATARI(q) { //TODO: сделать подсчет скидки более универсальным, передавать не кол-во, а состав корзины
  var discount = 0;
  var offvalue = false;
  var discountBlock = document.querySelector('#kart_discount');
  $(discountBlock).find('.active').removeClass('active').addClass('inactive');
  // if (this.cartCont.querySelector('#kart_discount')) {
  if (q < 2) {
    // this.cartCont.removeChild(this.cartCont.querySelector('#kart_discount'));
    discount = 0;
    offvalue = false;
  }
  if (q >= 2 && q < 4 ) {
    discount = 15;
    $(discountBlock).find('[data-cart-discount="' + discount + '"]').addClass('active').removeClass('inactive');
    offvalue = '15OFF';
  }
  if (q >= 4) {
    discount = 25;
    // $(this.discountBlock).find('.active').addClass('active');
    $(discountBlock).find('[data-cart-discount="' + discount + '"]').addClass('active').removeClass('inactive');
    offvalue = '25OFF';
  }
  return [discount, offvalue];
}


var notiShown = false;

$('[data-shop-kart=\'plural\']').each(function (i, item) {
  item.dataset.originalWord = item.textContent
});

function showCartNoti(thiss) {
  var notiEl = $(':attr(\'^data-kart-noti\')');
  var ixNotiHide = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 250ms ease 0ms',
      'x': '0px',
      'y': '80px',
      'z': '0px',
    }],
    'stepsB': []
  };

  if (thiss['q'] > 0) {
    setTimeout(function () {
      ix.run(ixResetXY, notiEl);
    }, 50)
  } else {
    setTimeout(function () {
      ix.run(ixNotiHide, notiEl);
    }, 50)
  }



  //Pluralize
  var $plurals = $('[data-shop-kart=\'plural\']');
  $plurals.each(function (i, item) {
    var word = item.dataset.originalWord || item.textContent;
    if (thiss['q'] > 1) {
      item.textContent = pluralize(word);
    } else {
      item.textContent = word;
    }
  });

}






//Эти идут
var shopSettings_LIF =
  {
    'theme': 'xxx_theme_lif',
    'cartSettings': {
      'indicatorShown': true, //auto
      'cartShown': 'auto', //auto
      'afterChange': typeof showCartNoti !== 'undefined' ? showCartNoti : null, //Callback when something changed in the cart
      'cartAddAnimation': typeof cartAddAnimation !== 'undefined' ? cartAddAnimation : null,
      'cartAddDiscount': typeof cartAddDiscount !== 'undefined' ? cartAddDiscount : null,
      'paystation': {
        'access_data': {
          'user': {
            'attributes': {
              // 'promo': false,
            }
          },
          'settings': {
            'project_id': 26233,
            // 'shipping_enabled': true
            'ui': {
              'size': 'medium',
              'theme': 'dark'
            },
          },
        },
      },
      'currency': ['$', 0],
    },



    'shopSettings': {
      // 'defaultGrids': 'all',
      // 'defaultGrids':
      //   [
      //     // { 'is_favorite': 1},
      //     {
      //       'groupId': 6721, //Consumables
      //       'title': 'All Other Goods',
      //       'template': 'template1'
      //     },
      //     {
      //       'groupId': 6724
      //     }, //Hats
      //     {
      //       'groupId': 6725
      //     }, //Boots
      //   ],
    },


    // 'staticData': lif_static_data, //IF STATIC DATA

    'customAttrs': phoenix_custom_attributes, //Custom Pictures and shipping
    'noDynamicStoreftont': false,
};



var ixKPlus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    // 'y': '-26px',
  }]
};



var ixKPlus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    // 'y': '-26px',
  }]
};

var ixKMinus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    // 'y': '26px',
  }]
};

var ixKPlusMinusOff = {
  // 'stepsA': [{
  //   'opacity': 0,
  //   'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  //   'scaleX': 1,
  //   'scaleY': 1,
  //   'scaleZ': 1,
  //   // 'y': '0px',
  // }]
};



var ix_qChange_1 = {
  'stepsA': [{
    'opacity': 1,
    'scale': 1,
    'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
  }]
};
var ix_qChange_0 = {
  'stepsA': [{
    'opacity': 1,
    'scale': 1,
    'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
  }]
};


var Xsolla;

// var dataStatic = phoenix_data;


function init1(access_data) {


  if (dataStatic) {

    shopSettings['shopData'] = dataStatic;
    shopSettings['cartSettings'].paystation.access_data.settings.project_id = access_data.settings.project_id;
    return;
  }

  $.ajax({
      url: 'https://secure.xsolla.com/paystation2/api/virtualitems/',
      data: {
          access_data: JSON.stringify(access_data)
      },
      method: 'post',
      dataType: 'json',
      beforeSend: function () {

      },
      success: function (items_data) {
          console.log(items_data);
          //document.querySelector('#jsn').innerText = JSON.stringify(data['groups'][0]['virtual_items']);
          //document.querySelector('#jsn').innerHTML = syntaxHighlight(data['groups'][0]['virtual_items']);
          //document.querySelector('.prettyprint').innerHTML = JSON.stringify(data['groups'][0]['virtual_items']);

        shopSettings['shopData'] = items_data['groups'];
        // shopSettings['cartSettings'].paystation.access_data.project_id = access_data.project_id;
        shopSettings['cartSettings'].paystation.access_data.settings.project_id = access_data.settings.project_id;
        // shopSettings['defaultGrids'] = [
        //   {
        //     'where': '#grid_mainpacks_new',
        //     'typeName': 'group',
        //     'typeValue': 'Mainpacks',
        //     'title': 'Phoenix Point Packs',
        //   },
        //   {
        //     'where': '#grid_addons',
        //     'typeName': 'group',
        //     'typeValue': 'Addons',
        //     'title': 'Phoenix Point Addons',
        //   },
        // ];


        // shop = new Shop(shopSettings);
        // xsolla = new Xsolla(shopSettings);


        //TEMP OLD STYLE
        // dataObj = shop._data;
        // drawItemsType('#grid_mainpacks_new', 'group', 'Mainpacks');
        // // drawItemsType('#grid_mainpacks', 'group', 'Mainpacks');
        // drawItemsType('#grid_addons', 'group', 'Addons');
        // shop.collectButtons();


        qq();

        addScrollTo(); //CLICK on buy scrolls


        // hideElements(itemsContainer);
      }
  });
}



// var access_data_phoenix = {
  //   "settings": {
    //     "project_id": 24042
    //   }
    // };

    // init(access_data_atari);
    // init(access_data_phoenix);



    //22877




////////////////////   PANEL   //////////////////////

// qq();
qq(true);

xsolla = new Xsolla(shopSettings_LIF, addScrollTo); //Callback



var xsolla;
function qq(hide) {
  // if (!xsolla) xsolla = new Xsolla(shop);
  if (hide) {
    $('.q').removeClass('shown');
    $('.b').removeClass('xsolla_panel');
    $('.float').removeClass('xsolla_panel');
    return
  }
  $('.q').addClass('shown');
  $('.b').addClass('xsolla_panel');
  $('.float').addClass('xsolla_panel');

}




$(':attr(\'^data-q-close\')').click(function () {
  qq(true)
})


$('.k').removeClass('shown');


